package com.github.piasy.base.model.database;

/**
 * Created by mikhailz on 11/06/2016.
 */

import android.database.Cursor;
import rx.Observable;

public class CursorObservable {
    /**
     * Create Observable that emits the specified {@link android.database.Cursor} for each available position
     * of the cursor moving to the next position before each call and closing the cursor whether the
     * Observable completes or an error occurs.
     */
    public static Observable<Cursor> fromCursor(final Cursor cursor) {
        return Observable.create(new OnSubscribeCursor(cursor));
    }
}