package com.github.piasy.base.utils;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by user_sca on 20.05.2015.
 */
public class ZipCompress {


    private File mFile;

    private File mLocationForOutput;

    public ZipCompress(File file, File locationForOutput) {
        mFile = file;
        mLocationForOutput = locationForOutput;
    }

    public File zip() {
        if (mFile == null) {
            return null;
        }

        List<File> fileList = new ArrayList<>();
        if (mFile.isDirectory()) {
            fileList.addAll(new ArrayList<>(Arrays.asList(mFile.listFiles())));
        } else {
            fileList.add(mFile);
        }

        try {

            File zipFile = new File(mLocationForOutput.getAbsolutePath() + "/" + mFile.getName() + ".zip");
            OutputStream os = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(os));
            try {
                for (File file : fileList) {
                    ZipEntry entry = new ZipEntry(file.getName());

                    zos.putNextEntry(entry);
                    writeToZip(file, zos);
                    zos.closeEntry();
                }
            } finally {
                zos.close();
                os.close();
            }

            return zipFile;
        } catch (IOException ex) {
            Log.e("Zip", ex.getLocalizedMessage());
            return null;
        }
    }

    public void writeToZip(File file, ZipOutputStream zos) throws IOException {

        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                zos.write(buffer, 0, read);
            }
        } finally {

            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
                Log.e("Zip", e.getLocalizedMessage());
            }
        }
    }
}
