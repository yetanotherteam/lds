package com.github.piasy.base.utils;

import com.github.piasy.base.model.database.CursorObservable;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by mikhailz on 13/06/2016.
 */
public class RxDatabaseUtils {


    @NonNull
    public static Func1<SQLiteDatabase, Cursor> getCursorFromQuery(final String query, final String[] args) {
        return new Func1<SQLiteDatabase, Cursor>() {
            @Override
            public Cursor call(SQLiteDatabase sqLiteDatabase) {
                return sqLiteDatabase.rawQuery(query, args, null);
            }
        };
    }


    public static Func1<Cursor, Observable<Cursor>> toCursorObservable() {
        return new Func1<Cursor, Observable<Cursor>>() {
            @Override
            public Observable<Cursor> call(Cursor cursor) {
                return CursorObservable.fromCursor(cursor);
            }
        };
    }


}
