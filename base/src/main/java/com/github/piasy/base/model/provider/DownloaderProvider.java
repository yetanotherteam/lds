package com.github.piasy.base.model.provider;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class DownloaderProvider {

    private static final String TAG = "DownloadProvider";


    private OkHttpClient mClient;

    @Inject
    public DownloaderProvider(OkHttpClient client) {
        mClient = client;
    }

    @RxLogObservable
    public Observable<File> downloadOrReturn(final DownloadRequest downloadRequest) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                final File file = new File(downloadRequest.getDownloadDir() + File.separator + downloadRequest.getName());

                Request request = new Request.Builder().url(downloadRequest.getUrl()).build();
                Response response = null;

                try {
                    response = mClient.newCall(request).execute();
                    File outFile = writeToFile(response, file);
                    subscriber.onNext(outFile);
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                } finally {
                    if (response != null) {
                        response.body().close();
                    }
                }

            }
        }).doOnNext(new Action1<File>() {
            @Override
            public void call(File file) {
                Timber.d("Download: success \n" + downloadRequest.getUrl() + "\n" +
                        "to: " + file.getAbsolutePath());
            }
        }).doOnCompleted(new Action0() {
            @Override
            public void call() {
                Timber.d("Download: finished \n" + downloadRequest.getUrl());
            }
        })
                .subscribeOn(Schedulers.trampoline());

    }

    ;

    private File writeToFile(Response response, File file) {
        BufferedSink sink = null;
        if (file.exists()) {
            Timber.d("File already exists, skipping...");
            return file;
        }

        final ResponseBody body = response.body();
        try {
            sink = Okio.buffer(Okio.sink(file));
            sink.writeAll(body.source());
        } catch (IOException e) {
            throw OnErrorThrowable.from(OnErrorThrowable.addValueAsLastCause(e, "Error"));
        } finally {
            try {
                response.body().close();
                if (sink != null) {
                    sink.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}