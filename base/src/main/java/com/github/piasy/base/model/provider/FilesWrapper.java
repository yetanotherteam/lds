package com.github.piasy.base.model.provider;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class FilesWrapper {
    private List<File> mFiles = new ArrayList<>();

    public FilesWrapper(List<File> files) {

        mFiles = files;
    }

    public FilesWrapper(File file) {
        mFiles.add(file);
    }

    public List<File> getFiles() {
        return mFiles;
    }
}
