package com.github.piasy.base.model.provider;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.github.piasy.base.utils.FileUtils;
import com.github.piasy.base.utils.ZipDecompress;

import android.support.annotation.NonNull;

import java.io.File;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

/**
 * Created by mikhailz on 11/06/2016.
 */
public class ArchiveProvider {

    private DownloaderProvider mDownloaderProvider;

    @Inject
    public ArchiveProvider(DownloaderProvider downloaderProvider) {
        mDownloaderProvider = downloaderProvider;
    }

    public Observable<File> downloadUrl(final String url) {

        return Observable.defer(new Func0<Observable<File>>() {
            @Override
            public Observable<File> call() {
                return mDownloaderProvider.downloadOrReturn(getDownloadRequest(url));
            }
        });
    }

    @RxLogObservable
    public Observable<File> downloadUrlAndUnzip(final String url, final String ... onlyFiles) {

        return Observable.defer(new Func0<Observable<File>>() {
            @Override
            public Observable<File> call() {
                return downloadUrl(url);
            }
        }).flatMap(new Func1<File, Observable<File>>() {
            @Override
            public Observable<File> call(File file) {
                return ZipDecompress.unzip(file, Constants.unzipFolder, onlyFiles);
            }
        });
    }

    @RxLogObservable
    public Observable<File> downloadUrlUzipAndAllFiles(final String url) {
        return Observable.defer(new Func0<Observable<File>>() {
            @Override
            public Observable<File> call() {
                return downloadUrlAndUnzip(url);
            }
        }).flatMap(new Func1<File, Observable<File>>() {
            @Override
            public Observable<File> call(File file) {
                return Observable.from(file.listFiles());
            }
        });
    }

    @RxLogObservable
    public Observable<File> downloadUrlUnzipAndGetFile(final String url, final String fileName) {
        return Observable.defer(new Func0<Observable<File>>() {
            @Override
            public Observable<File> call() {
                return downloadUrlUzipAndAllFiles(url);
            }
        }).filter(new Func1<File, Boolean>() {
            @Override
            public Boolean call(File file) {
                return file.getName().equals(fileName);
            }
        });
    }

    @NonNull
    @RxLogObservable
    public static Func1<File, Observable<File>> unzip(final String... fileNames) {
        return new Func1<File, Observable<File>>() {
            @Override
            public Observable<File> call(final File file) {
                return Observable.defer(new Func0<Observable<File>>() {
                    @Override
                    public Observable<File> call() {
                        return ZipDecompress.unzip(
                                file,
                                new File(Constants.unzipFolder,
                                        FileUtils.getUnzipFolderNameFromArhiveFile(file)), fileNames);
                    }
                });
            }
        };
    }

    public static DownloadRequest getDownloadRequest(String url) {
        return new DownloadRequest(
                url,
                getFileWithoutExtentsionFromUrl(url),
                Constants.downloadFolder.getAbsolutePath());
    }

    @NonNull
    public static String getFileWithoutExtentsionFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }
}
