package com.github.piasy.base.model.provider;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class DownloadRequest {
    private String mUrl;
    private String mName;
    private String mDownloadDir;

    public DownloadRequest(String url, String name, String downloadDir) {
        mUrl = url;
        mName = name;
        mDownloadDir = downloadDir;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getName() {
        return mName;
    }

    public String getDownloadDir() {
        return mDownloadDir;
    }
}
