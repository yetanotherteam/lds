/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Piasy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.github.piasy.base.model.provider;

import com.google.gson.Gson;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Module
public class ProviderModule {

    @Singleton
    @Provides
    EventBus provideEventBus(final EventBusProvider.Config config) {
        return EventBusProvider.provideEventBus(config);
    }

    @Singleton
    @Provides
    Gson provideGson(final GsonProvider.Config config) {
        return GsonProvider.provideGson(config);
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(final RetrofitProvider.Config config, final OkHttpClient okHttpClient,
                             final Gson gson) {
        return RetrofitProvider.provideRetrofit(config, okHttpClient, gson);
    }

    @Singleton
    @Provides
    OkHttpClient provideHttpClient(final HttpClientProvider.Config config) {
        return provideHttpClientInternal(config);
    }

    /**
     * Override this method to provide mock http client in test app.
     */
    protected OkHttpClient provideHttpClientInternal(final HttpClientProvider.Config config) {
        return HttpClientProvider.provideHttpClient(config);
    }

    @Singleton
    @Provides
    StorIOSQLite provideStorIOSQLite(final StorIOSQLiteProvider.Config config) {
        return StorIOSQLiteProvider.provideStorIOSQLite(config);
    }

}
