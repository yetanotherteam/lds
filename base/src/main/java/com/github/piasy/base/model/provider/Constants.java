package com.github.piasy.base.model.provider;

import com.github.piasy.base.utils.FileUtils;

import java.io.File;

/**
 * Created by mikhailz on 09/06/2016.
 */
public class Constants {

    public static File downloadFolder = FileUtils.getDocDir("my");
    public static File unzipFolder = FileUtils.getDocDir("myUnzip");
}
