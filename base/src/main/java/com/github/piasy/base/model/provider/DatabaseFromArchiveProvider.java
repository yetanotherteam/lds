package com.github.piasy.base.model.provider;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by mikhailz on 12/06/2016.
 */
public class DatabaseFromArchiveProvider {

    private ArchiveProvider mArchiveProvider;

    public static Map<String, Observable<SQLiteDatabase>> dbConnections = new HashMap<>();

    @Inject
    public DatabaseFromArchiveProvider(ArchiveProvider archiveProvider) {
        mArchiveProvider = archiveProvider;
    }

    @RxLogObservable
    public Observable<SQLiteDatabase> getDatabase(String url, String dbFileName) {

        if (dbConnections.containsKey(dbFileName)) {
            return dbConnections.get(dbFileName);
        }

        Observable<SQLiteDatabase> observable = mArchiveProvider.downloadUrlUnzipAndGetFile(url, dbFileName)
                .map(new Func1<File, SQLiteDatabase>() {
                    @Override
                    public SQLiteDatabase call(File file) {
                        return SQLiteDatabase.openDatabase(file.getAbsolutePath(), null, 0);
                    }
                }).cache();

        dbConnections.put(dbFileName, observable);

        return observable;
    }
}
