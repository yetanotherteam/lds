package com.github.piasy.base.utils;

/**
 * Created by user_sca on 12.05.2015.
 */

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import timber.log.Timber;

public class ZipDecompress {

    /**
     * srcFile - source file
     * location - out location dir
     * fileNames - if not nulll only those files will be unzipped
     *
     * @return true if unzipped sucessfully
     */
    @RxLogObservable
    public static Observable<File> unzip(final File srcFile, final File location, final String... fileNames) {

        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                try {
                    if (!subscriber.isUnsubscribed()) {
                        try {
                            final File unzipDir = unzipAction(srcFile, location, fileNames);
                            subscriber.onNext(unzipDir);
                        } catch (Exception ex) {
                            subscriber.onError(ex);
                        }
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                    subscriber.onCompleted();
                    Timber.e(e.getLocalizedMessage());
                }
            }
        }).doOnNext(new Action1<File>() {
            @Override
            public void call(File file) {
                Timber.d("Unzipping: starting" + file.getAbsolutePath());
            }
        });
    }

    /**
     * @param srcFile  - input file
     * @param location - out dir
     */
    private synchronized static File unzipAction(File srcFile, File location, String... fileNames) throws Exception {

        new File(location.getAbsolutePath()).mkdirs();

        FileInputStream fin = new FileInputStream(srcFile);
        ZipInputStream zin = new ZipInputStream(fin);
        ZipEntry ze = null;
        while ((ze = zin.getNextEntry()) != null) {
            Timber.v("Decompress", "Unzipping " + ze.getName());

            if (ze.isDirectory()) {
                File f = new File(location + File.separator + ze.getName());

                if (!f.isDirectory()) {
                    f.mkdirs();
                }

            } else {
                String outFileName = location.getAbsolutePath() + File.separator + ze.getName();

                if (fileNames != null && fileNames.length != 0) {
                    List<String> strings = Arrays.asList(fileNames);
                    if (!strings.isEmpty() && !strings.contains(ze.getName())) {
                        Timber.d("File not found: " + ze.getName());
                        break;
                    }
                }

                if (new File(outFileName).exists()) {
                    Timber.d("Already exists, returninig...");
                    continue;
                }
                FileOutputStream fout = new FileOutputStream(outFileName);

                byte[] buffer = new byte[32 * 1024];
                int readCount;
                while ((readCount = zin.read(buffer)) != -1) {
                    fout.write(buffer, 0, readCount);
                }

                zin.closeEntry();
                fout.close();
            }

        }
        zin.close();
        return location;


    }
}