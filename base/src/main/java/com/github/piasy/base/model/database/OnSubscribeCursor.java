package com.github.piasy.base.model.database;

/**
 * Created by mikhailz on 11/06/2016.
 */
import android.database.Cursor;

import rx.Observable;
import rx.Subscriber;

/**
 * Emits a {@link android.database.Cursor} for every available position.
 */
public class OnSubscribeCursor implements Observable.OnSubscribe<Cursor> {

    private final Cursor cursor;

    OnSubscribeCursor(final Cursor cursor) {
        this.cursor = cursor;
    }

    @Override
    public void call(final Subscriber<? super Cursor> subscriber) {
        try {
            while (!subscriber.isUnsubscribed() && cursor.moveToNext()) {
                subscriber.onNext(cursor);
            }
            if (!subscriber.isUnsubscribed()) {
                subscriber.onCompleted();
            }
        } catch (Throwable e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
            }
        } finally {
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
    }

}