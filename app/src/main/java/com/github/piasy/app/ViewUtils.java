package com.github.piasy.app;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.Random;

/**
 * Created by mikhailz on 11/05/16.
 */
public final class ViewUtils {
    private ViewUtils() {
    }

    public static void applyOnClickListener(View.OnClickListener onClickListener, View... views) {
        for (View v : views) {
            v.setOnClickListener(onClickListener);
        }
    }

    public static float convertPixelsToDp(float px) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static int generateViewId() {
        return randInt(2, 65500);
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
