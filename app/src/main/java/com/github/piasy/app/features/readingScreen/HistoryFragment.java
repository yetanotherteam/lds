package com.github.piasy.app.features.readingScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.features.BaseMvpFragmentLce;
import com.github.piasy.app.features.readingScreen.di.ReadingComponent;
import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenter;
import com.github.piasy.model.users.HistoryItems;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 26/05/2016.
 */
public class HistoryFragment extends BaseMvpFragmentLce<ReadingView, ReadingPresenter,
        ReadingComponent, RecyclerView, HistoryItems> {

    private HistoryItems mData;
    private RecyclerView mRecyclerView;

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = ButterKnife.findById(view, R.id.contentView);

        getPresenter().startLoadingHistory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.history_list;
    }

    @Override
    public void showContent() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new HistoryAdapter(mData, new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

            }
        }));
        super.showContent();
    }

    @Override
    public void setData(HistoryItems data) {
        mData = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    private class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {


        private HistoryItems mData;
        private HistoryFragment.OnItemClickListener mOnItemClickListener;


        public HistoryAdapter(HistoryItems data, HistoryFragment.OnItemClickListener onClickListener) {
            mData = data;
            mOnItemClickListener = onClickListener;
        }


        @Override
        public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.history_list_row, parent, false);

            return new HistoryViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(HistoryViewHolder holder, int position) {
            holder.getContent().setText(mData.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class HistoryViewHolder extends RecyclerView.ViewHolder {

            private final TextView mContent;
            private final TextView mRightButton;

            public HistoryViewHolder(View itemView) {
                super(itemView);
                mContent = ButterKnife.findById(itemView, R.id.tvBookmarkTitle);
                mRightButton = ButterKnife.findById(itemView, R.id.rightButton);
                mRightButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                });
            }

            public TextView getContent() {
                return mContent;
            }

            public TextView getRightButton() {
                return mRightButton;
            }
        }
    }


}
