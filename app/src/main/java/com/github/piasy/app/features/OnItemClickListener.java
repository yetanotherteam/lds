package com.github.piasy.app.features;

import android.view.View;

/**
 * Created by mikhailz on 01/06/2016.
 */

public interface OnItemClickListener {
    void onItemClick(View itemView, int position);
}
