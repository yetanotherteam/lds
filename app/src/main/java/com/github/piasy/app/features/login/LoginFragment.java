package com.github.piasy.app.features.login;

import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.github.piasy.app.BuildConfig;
import com.github.piasy.app.R;
import com.github.piasy.app.ViewUtils;
import com.github.piasy.app.features.goals.GoalsActivity;
import com.github.piasy.app.features.login.di.FacebookProvider;
import com.github.piasy.app.features.login.di.LoginComponent;
import com.github.piasy.app.features.login.mvp.LoginPresenter;
import com.github.piasy.app.features.login.mvp.LoginView;
import com.github.piasy.app.features.readingScreen.ReadingControlActivity;
import com.github.piasy.app.features.splash.BlurBackground;
import com.github.piasy.base.android.BaseMvpFragment;
import com.github.piasy.model.users.LoginResponse;
import com.github.piasy.model.users.SignUpResponse;
import com.github.piasy.model.users.Success;
import com.yatatsu.autobundle.AutoBundleField;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.ButterKnife;
import jonathanfinerty.once.Once;

/**
 * Created by mikhailz on 09/05/16.
 */
public class LoginFragment
        extends BaseMvpFragment<LoginView, LoginPresenter, LoginComponent>
        implements LoginView {

    private final OnStartLoginListener mOnStartLoginListener = new OnStartLoginListener() {
        @Override
        public void startLogin(String user, String password) {
            getPresenter().loginWithUserName(user, password);
        }

        @Override
        public void startSignUp(String user, String password, String email) {
            getPresenter().signupWithUserName(user, password, email);
        }
    };
    @AutoBundleField
    LoginActivty.Type mType;
    @Inject
    FacebookProvider mFacebookProvider;
    private TextView mTvActionLds;
    private TextView mTvAction;
    private TextView mTvSuggestion;
    private Button mFacebookButton;
    private ProgressDialog mProgressDialog;
    private TextView mTvMessageError;
    private ImageView mIvBackGround;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int id = v.getId();

            if (id == R.id.tvActionLds) {
                startLdsAction(mType);
            } else if (id == R.id.tvAction) {
                switch (mType) {
                    case Login:
                        startLdsAction(LoginActivty.Type.SignUp);
                        break;
                    case SignUp:
                        startLdsAction(LoginActivty.Type.Login);
                        break;
                    default:
                        break;
                }
            } else if (id == R.id.facebook_login_button) {
                final ArrayList<String> strings = new ArrayList<>();
                strings.add("public_profile");
                strings.add("email");
                LoginManager.getInstance().logInWithReadPermissions(LoginFragment.this, strings);

            }
        }
    };


    @Override
    protected boolean hasArgs() {
        return true;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override
    protected void bindView(final View rootView) {
        mTvActionLds = ButterKnife.findById(rootView, R.id.tvActionLds);
        mTvAction = ButterKnife.findById(rootView, R.id.tvAction);
        mTvSuggestion = ButterKnife.findById(rootView, R.id.tvSuggestion);
        mFacebookButton = ButterKnife.findById(rootView, R.id.facebook_login_button);
        mTvMessageError = ButterKnife.findById(rootView, R.id.tvMessageError);
        mIvBackGround = ButterKnife.findById(rootView, R.id.ivBackground);
    }

    void startLdsAction(LoginActivty.Type type) {
        mTvMessageError.setText("");
        DialogFragment dialogFragment = null;

        switch (type) {
            case Login:
                dialogFragment = new LdsLoginDialogFragment();
                ((LdsLoginDialogFragment) dialogFragment).setOnStartActionListener(mOnStartLoginListener);
                break;
            case SignUp:
                dialogFragment = new LdsSignupDialogFragment();
                ((LdsSignupDialogFragment) dialogFragment).setOnStartActionListener(mOnStartLoginListener);
                break;
            default:
                break;
        }
        dialogFragment.setCancelable(false);
        dialogFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialog = new ProgressDialog(getActivity());
        mTvActionLds.setText(R.string.ldsscriptures_action);

        switch (mType) {
            case Login:
                mFacebookButton.setText(R.string.login_in_with_facebook);
                mTvAction.setText(R.string.action_signup);
                mTvSuggestion.setText(R.string.need_an_account);
                break;
            case SignUp:
                mFacebookButton.setText(R.string.sign_in_with_facebook);
                mTvAction.setText(R.string.action_login);
                mTvSuggestion.setText(R.string.have_an_account);
                break;

            default:
                break;
        }

        mIvBackGround.setImageBitmap(BlurBackground.getInstanse().getBitmap());

        ViewUtils.applyOnClickListener(mOnClickListener, mTvActionLds, mTvAction, mFacebookButton);


        if (BuildConfig.DEBUG) {
//            getPresenter().loginWithUserName("q@mail.ru", "12345678");
            final Intent intent = new Intent(getActivity(), ReadingControlActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @UiThread
    @Override
    public void showStartLogining() {
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.show();
    }

    @UiThread
    @Override
    public void showStopLogining() {
        mProgressDialog.cancel();
    }

    @UiThread
    @Override
    public void showLoginedState(LoginResponse loginResponse) {

        if (loginResponse.success() == Success.FALSE.getValue()) {
            showCredsErrorState(loginResponse.msg());
        } else {
            if (Once.beenDone(GoalsActivity.DONE_TAG)) {
                final Intent intent = new Intent(getActivity(), ReadingControlActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                final Intent intent = new Intent(getActivity(), GoalsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        }
    }

    @UiThread
    @Override
    public void showCredsErrorState(String msg) {
        if (msg != null) {
            mTvMessageError.setCompoundDrawables(getResources().getDrawable(android.R.drawable.stat_notify_error),
                    null, null, null);
            mTvMessageError.setText(msg);
        }

    }

    @UiThread
    @Override
    public void showNetworkErrorState() {
        mTvMessageError.setCompoundDrawables(getResources().getDrawable(android.R.drawable.stat_notify_error),
                null, null, null);
        mTvMessageError.setText(getString(R.string.network_error));
    }

    @Override
    public void loginWithSignedUpState(final SignupRequestImpl request,
                                       final SignUpResponse signUpResponse) {
        if (signUpResponse.success() == Success.TRUE.getValue()) {
            getPresenter().loginWithUserName(request.email(), request.password());

        } else {
            getMvpView().showCredsErrorState(signUpResponse.msg());
        }
    }

    @UiThread
    @Override
    public void goSignupWindowAndFillProfile(Profile profile) {
        final LdsSignupDialogFragment dialogFragment = LdsSignupDialogFragmentAutoBundle.createFragmentBuilder(profile)
                .build();
        dialogFragment.setOnStartActionListener(mOnStartLoginListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getPresenter().onActivityResult(requestCode, resultCode, data);

    }
}
