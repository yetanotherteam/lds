package com.github.piasy.app.features.login;

import com.google.auto.value.AutoValue;

import com.github.piasy.app.features.login.di.IRequest;

import android.app.Activity;

/**
 * Created by mikhailz on 19/05/2016.
 */
@AutoValue
public abstract class FacebookLoginRequestImpl implements IRequest {

    public static Builder builder() {
        return new AutoValue_FacebookLoginRequestImpl.Builder();
    }

    public abstract Activity activity();

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder activity(Activity activity);

        public abstract FacebookLoginRequestImpl build();
    }
}
