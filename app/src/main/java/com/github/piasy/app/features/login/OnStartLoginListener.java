package com.github.piasy.app.features.login;

/**
 * Created by mikhailz on 11/05/16.
 */
public interface OnStartLoginListener {
    void startLogin(String user, String password);

    void startSignUp(String user, String password, String email);
}
