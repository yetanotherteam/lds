package com.github.piasy.app.features.readingScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.ViewUtils;
import com.github.piasy.app.features.splash.BlurBackground;
import com.joanzapata.iconify.widget.IconTextView;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.ImageView;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 09/05/16.
 */
@SuppressWarnings({
        "PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity",
        "PMD.ModifiedCyclomaticComplexity"
})
public class MenuBottomSheetFragment extends BottomSheetDialogFragment {


    private BottomSheetBehavior mBehavior;


    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback =
            new BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        dismiss();
                    }

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.main_bottom_sheet, null);

        dialog.setContentView(contentView);

        mBehavior = BottomSheetBehavior.from((View) contentView.getParent());

        final Bitmap bitmap = BlurBackground.getInstanse().getBitmap();

        final ImageView background = ButterKnife.findById(contentView, R.id.ivBackground);
        if (bitmap != null) {
            background.setImageBitmap(bitmap);
        } else {
            background.setImageResource(R.mipmap.milkyway);
        }

        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bindView(contentView);

    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    protected void bindView(final View rootView) {
        final IconTextView btClose = ButterKnife.findById(rootView, R.id.closeButton);
        final IconTextView btSettings = ButterKnife.findById(rootView, R.id.settingsButton);
        final TextViewIconCheckable btAchivements = ButterKnife.findById(rootView, R.id.btAchivements);
        final TextViewIconCheckable btActivity = ButterKnife.findById(rootView, R.id.btActivity);
        final TextViewIconCheckable btArt = ButterKnife.findById(rootView, R.id.btArt);
        final TextViewIconCheckable btArtefacts = ButterKnife.findById(rootView, R.id.btArtefacts);
        final TextViewIconCheckable btDaily = ButterKnife.findById(rootView, R.id.btDaily);
        final TextViewIconCheckable btFriends = ButterKnife.findById(rootView, R.id.btFriends);
        final TextViewIconCheckable btHistory = ButterKnife.findById(rootView, R.id.btHistory);
        final TextViewIconCheckable btJournal = ButterKnife.findById(rootView, R.id.btJournal);
        final TextViewIconCheckable btLeaderbord = ButterKnife.findById(rootView, R.id.btLeaderbord);
        final TextViewIconCheckable btLessons = ButterKnife.findById(rootView, R.id.btLessons);
        final TextViewIconCheckable btLibrary = ButterKnife.findById(rootView, R.id.btLibrary);
        final TextViewIconCheckable btNotes = ButterKnife.findById(rootView, R.id.btNotes);
        final TextViewIconCheckable btPlans = ButterKnife.findById(rootView, R.id.btPlans);
        final TextViewIconCheckable btRandom = ButterKnife.findById(rootView, R.id.btRandom);
        final TextViewIconCheckable btTags = ButterKnife.findById(rootView, R.id.btTags);
        final TextViewIconCheckable btReadingNow = ButterKnife.findById(rootView, R.id.btReadingNow);
        final TextViewIconCheckable btTimeline = ButterKnife.findById(rootView, R.id.btTimeline);
        final TextViewIconCheckable btTravelogue = ButterKnife.findById(rootView, R.id.btTravelogue);
        final TextViewIconCheckable btVideo = ButterKnife.findById(rootView, R.id.btVideo);


        ViewUtils.applyOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               final int id = v.getId();
                                               if (id == btAchivements.getId()) {

                                               } else if (id == btActivity.getId()) {

                                               } else if (id == btArt.getId()) {

                                               } else if (id == btArtefacts.getId()) {

                                               } else if (id == btDaily.getId()) {

                                               } else if (id == btFriends.getId()) {

                                               } else if (id == btHistory.getId()) {

                                               } else if (id == btJournal.getId()) {

                                               } else if (id == btLeaderbord.getId()) {

                                               } else if (id == btLessons.getId()) {

                                               } else if (id == btLibrary.getId()) {

                                               } else if (id == btNotes.getId()) {

                                               } else if (id == btPlans.getId()) {

                                               } else if (id == btRandom.getId()) {

                                               } else if (id == btTags.getId()) {

                                               } else if (id == btReadingNow.getId()) {

                                               } else if (id == btTimeline.getId()) {

                                               } else if (id == btTravelogue.getId()) {

                                               } else if (id == btVideo.getId()) {

                                               } else if (id == btClose.getId()) {
                                               } else if (id == btSettings.getId()) {

                                               }
                                               mBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                           }
                                       },
                btAchivements, btActivity, btArt, btArtefacts, btDaily,
                btFriends, btHistory, btJournal, btLeaderbord, btLessons,
                btLibrary, btNotes, btPlans, btRandom, btTags, btReadingNow,
                btTimeline, btTravelogue, btVideo, btClose, btSettings);


    }

    @Override
    public void onDetach() {

        super.onDetach();
    }
}
