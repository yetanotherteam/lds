package com.github.piasy.app.features.readingScreen.mvp;

import com.github.piasy.app.IO;
import com.github.piasy.app.features.readingScreen.ReadingView;
import com.github.piasy.base.model.database.CursorObservable;
import com.github.piasy.base.model.provider.DatabaseFromArchiveProvider;
import com.github.piasy.base.mvp.NullObjRxBasePresenter;
import com.github.piasy.model.catalog.Item;
import com.github.piasy.model.catalog.Item_category;
import com.github.piasy.model.catalog.dao.DbCategoryMetaTable;
import com.github.piasy.model.errors.RxNetErrorProcessor;
import com.github.piasy.model.users.BookMeta;
import com.github.piasy.model.users.Bookmark;
import com.github.piasy.model.users.Bookmarks;
import com.github.piasy.model.users.GenericList;
import com.github.piasy.model.users.HistoryItem;
import com.github.piasy.model.users.HistoryItems;
import com.github.piasy.model.users.daoBook.DbBookItemDao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by mikhailz on 26/05/2016.
 */
public class ReadingPresenterImpl extends NullObjRxBasePresenter<ReadingView> implements ReadingPresenter {


    private RxNetErrorProcessor mRxNetErrorProcessor;
    private DbBookItemDao mDbBookItemDao;
    private DatabaseFromArchiveProvider mDatabaseFromArchiveProvider;
    private Observable<SQLiteDatabase> sqLiteDatabaseObservable;

    @Inject
    public ReadingPresenterImpl(
            RxNetErrorProcessor rxNetErrorProcessor,
            DbBookItemDao dbBookItemDao,
            DatabaseFromArchiveProvider databaseFromArchiveProvider) {

        mRxNetErrorProcessor = rxNetErrorProcessor;
        mDbBookItemDao = dbBookItemDao;
        mDatabaseFromArchiveProvider = databaseFromArchiveProvider;

        initDatabase();
    }

    @Override
    public void initDatabase() {
        sqLiteDatabaseObservable = getSQLiteDatabaseObservable().cache();
    }

    @Override
    public void startLoadingBookmarks() {
        final Bookmarks bookmarks = new Bookmarks();
        Bookmark bookmark = new Bookmark("Bookmark");
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);
        bookmarks.add(bookmark);


        addSubscription(
                IO.subscribe(
                        IO.showAtLceObservable(getView(),
                                Observable.just(bookmarks)),
                        new Action1<Bookmarks>() {
                            @Override
                            public void call(Bookmarks bookmarks) {

                            }
                        },
                        mRxNetErrorProcessor)
        );
    }

//
//    public Observable<Library_collection> getLibraryCollectionsForRoot() {
//
//        return Observable.zip(mDbBookItemDao.getBooksMeta(), sqLiteDatabaseObservable, new Func2<BookMeta, SQLiteDatabase, Cursor>() {
//            @Override
//            public Cursor call(BookMeta bookMeta, SQLiteDatabase sqLiteDatabase) {
//                return sqLiteDatabase.rawQuery(DbCategoryMetaTable.librarySections,
//                        new String[]{String.valueOf(bookMeta.root_library_collection_id())});
//            }
//        }).map(Library_collection.mapper());
//
//    }
//
//    public Observable<Library_collection> getLibraryCollection(Observable<Library_collection> library_collectionObservableForRoot) {
//        return Observable.zip(library_collectionObservableForRoot, sqLiteDatabaseObservable, new Func2<Library_collection, SQLiteDatabase, Cursor>() {
//            @Override
//            public Cursor call(Library_collection library_collection, SQLiteDatabase sqLiteDatabase) {
//                return sqLiteDatabase.rawQuery(DbCategoryMetaTable.libraryCollections,
//                        new String[]{String.valueOf(library_collection._id())});
//            }
//        }).map(Library_collection.mapper());
//
//    }
//
//    public Observable<Library_section> getGroups() {
//        return Observable.zip(getLibraryCollection(getLibraryCollectionsForRoot()).first(),
//                sqLiteDatabaseObservable, new Func2<Library_collection, SQLiteDatabase, Cursor>() {
//                    @Override
//                    public Cursor call(Library_collection library_collection, SQLiteDatabase sqLiteDatabase) {
//                        return sqLiteDatabase.rawQuery(DbCategoryMetaTable.groupSectionsForCollection,
//                                new String[]{String.valueOf(library_collection._id())});
//                    }
//                }).map(Library_section.mapper());
//    }
//
//    public Observable<Library_collection> getGroupsCollection(){
//        return Observable.zip(mDbBookItemDao.getBooksMeta(), getGroups().first(), sqLiteDatabaseObservable,
//                new Func3<BookMeta, Library_section, SQLiteDatabase, Cursor>() {
//                    @Override
//                    public Cursor call(BookMeta bookMeta, Library_section library_section, SQLiteDatabase sqLiteDatabase) {
//                         return sqLiteDatabase.rawQuery(DbCategoryMetaTable.libraryCollections,
//                                new String[]{String.valueOf(library_section._id())});
//                    }
//                }
//        ).map(Library_collection.mapper());
//    }
//
//    public Observable<Library_item> getItemsForSection(){
//        return Observable.zip(getGroups().first(), sqLiteDatabaseObservable, new Func2<Library_section, SQLiteDatabase, Cursor>() {
//            @Override
//            public Cursor call(Library_section library_section, SQLiteDatabase sqLiteDatabase) {
//                return sqLiteDatabase.rawQuery(DbCategoryMetaTable.librarySections,
//                        new String[]{String.valueOf(library_section._id())});
//            }
//        }).map(Library_item.mapper());
//    }

    @Override
    public void startCategoryLoading() {

        Observable<GenericList> listObservable = sqLiteDatabaseObservable
                .flatMap(new Func1<SQLiteDatabase, Observable<Cursor>>() {
                    @Override
                    public Observable<Cursor> call(SQLiteDatabase sqLiteDatabase) {
                        Cursor cursor = sqLiteDatabase.rawQuery(DbCategoryMetaTable.QUERY_ALL_CATEGORIES, null);
                        return CursorObservable.fromCursor(cursor);
                    }
                }).map(Item_category.mapper()).toList()
                .map(new Func1<List<Item_category>, GenericList>() {
                    @Override
                    public GenericList call(List<Item_category> item_categories) {
                        return new GenericList<>(Item_category.class, item_categories);
                    }
                });

        Subscription loadSubscribtion = IO.subscribe(
                IO.showAtLceObservable(getView(),
                        listObservable),
                new Action1() {
                    @Override
                    public void call(Object o) {

                    }
                },
                mRxNetErrorProcessor);

        addSubscription(loadSubscribtion);
    }

    public Observable<SQLiteDatabase> getSQLiteDatabaseObservable() {
        return mDbBookItemDao.getBooksMeta()
                .flatMap(new Func1<BookMeta, Observable<SQLiteDatabase>>() {
                    @Override
                    public Observable<SQLiteDatabase> call(BookMeta bookMeta) {
                        return mDatabaseFromArchiveProvider.getDatabase(bookMeta.catalog_url(), "Catalog.sqlite");
                    }
                });
    }

    @Override
    public void startLoadingHistory() {

        final HistoryItems historyItems = new HistoryItems();
        final HistoryItem historyItem = new HistoryItem("History item");
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);
        historyItems.add(historyItem);


        addSubscription(
                IO.subscribe(
                        IO.showAtLceObservable(getView(),
                                Observable.just(historyItems)),
                        new Action1<HistoryItems>() {
                            @Override
                            public void call(HistoryItems bookmarks) {

                            }
                        },
                        mRxNetErrorProcessor)
        );
    }

    @Override
    public void startLoadingItems(final Long categoryId) {
        Observable<GenericList> genericListObservable = sqLiteDatabaseObservable
                .flatMap(new Func1<SQLiteDatabase, Observable<Cursor>>() {
                    @Override
                    public Observable<Cursor> call(SQLiteDatabase sqLiteDatabase) {
                        Cursor cursor = sqLiteDatabase.rawQuery(DbCategoryMetaTable.QUERY_ALL_ITEMS_IN_CATEGORY, new String[]{String.valueOf(categoryId)});
                        return CursorObservable.fromCursor(cursor);
                    }
                }).map(Item.mapper()).toList()
                .map(new Func1<List<Item>, GenericList>() {
                    @Override
                    public GenericList<Item> call(List<Item> items) {
                        return new GenericList<>(Item.class, items);
                    }
                });

        Subscription loadSubscribtion = IO.subscribe(
                IO.showAtLceObservable(getView(),
                        genericListObservable),
                new Action1() {
                    @Override
                    public void call(Object o) {

                    }
                },
                mRxNetErrorProcessor);
        addSubscription(loadSubscribtion);

    }


}
