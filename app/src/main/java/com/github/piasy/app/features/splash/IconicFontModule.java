package com.github.piasy.app.features.splash;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconFontDescriptor;

/**
 * Created by mikhailz on 24/05/2016.
 */
public class IconicFontModule implements IconFontDescriptor {
    @Override
    public String ttfFileName() {
        return "fonts/google-material-iconic-font-v1.ttf";
    }

    @Override
    public Icon[] characters() {
        return IconicIcons.values();
    }
}
