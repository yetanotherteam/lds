package com.github.piasy.app.features;

import com.github.piasy.app.R;
import com.github.piasy.base.android.BaseMvpFragment;
import com.github.piasy.base.di.BaseMvpComponent;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;
import com.hannesdorfmann.mosby.mvp.lce.LceAnimator;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by mikhailz on 27/05/2016.
 */

/**
 * @param <V> - mvp view
 * @param <P> - presenter
 * @param <C> - component
 * @param <U> - content view
 * @param <M> - model type
 */
public abstract class BaseMvpFragmentLce
        <V extends MvpView, P extends MvpPresenter<V>,
                C extends BaseMvpComponent<V, P>, U extends View, M>
        extends BaseMvpFragment<V, P, C>
        implements MvpLceView<M> {


    protected View mLoadingView;
    protected U mContentView;
    protected TextView mErrorView;


    public Integer getLoadingViewId() {
        return null;
    }

    public Integer getContentViewId() {
        return null;
    }

    public Integer getErrorViewId() {
        return null;
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final int loadingId = getLoadingViewId() != null ? (int) getLoadingViewId() : R.id.loadingView;
        final int contentId = getContentViewId() != null ? (int) getContentViewId() : R.id.contentView;
        final int errorId = getErrorViewId() != null ? (int) getErrorViewId() : R.id.errorView;
        mLoadingView = view.findViewById(loadingId);
        mContentView = (U) view.findViewById(contentId);
        mErrorView = (TextView) view.findViewById(errorId);

        if (mLoadingView == null) {
            throw new NullPointerException(
                    "Loading view is null! Have you specified a loading view in your layout xml file?" +
                            " You have to give your loading View the id R.id.mLoadingView");
        }

        if (mContentView == null) {
            throw new NullPointerException(
                    "Content view is null! Have you specified a content view in your layout xml file?" +
                            " You have to give your content View the id R.id.mContentView");
        }

        if (mErrorView == null) {
            throw new NullPointerException(
                    "Error view is null! Have you specified a content view in your layout xml file?" +
                            " You have to give your error View the id R.id.mErrorView");
        }

        mErrorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onErrorViewClicked();
            }
        });
    }

    @Override
    public void showLoading(boolean pullToRefresh) {

        if (!pullToRefresh) {
            animateLoadingViewIn();
        }

        // otherwise the pull to refresh widget will already display a loading animation
    }

    /**
     * Override this method if you want to provide your own animation for showing the loading view
     */
    protected void animateLoadingViewIn() {
        LceAnimator.showLoading(mLoadingView, mContentView, mErrorView);
    }

    @Override
    public void showContent() {
        animateContentViewIn();
    }

    /**
     * Called to animate from loading view to content view
     */
    protected void animateContentViewIn() {
        LceAnimator.showContent(mLoadingView, mContentView, mErrorView);
    }

    /**
     * Get the error message for a certain Exception that will be shown on {@link
     * #showError(Throwable, boolean)}
     */
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.loading_error);
    }

    /**
     * The default behaviour is to display a toast message as light error (i.e. pull-to-refresh
     * error).
     * Override this method if you want to display the light error in another way (like crouton).
     */
    protected void showLightError(String msg) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Called if the error view has been clicked. To disable clicking on the mErrorView use
     * <code>mErrorView.setClickable(false)</code>
     */
    protected void onErrorViewClicked() {
        loadData(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {

        String errorMsg = getErrorMessage(e, pullToRefresh);

        if (pullToRefresh) {
            showLightError(errorMsg);
        } else {
            mErrorView.setText(errorMsg);
            animateErrorViewIn();
        }
    }

    /**
     * Animates the error view in (instead of displaying content view / loading view)
     */
    protected void animateErrorViewIn() {
        LceAnimator.showErrorView(mLoadingView, mContentView, mErrorView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLoadingView = null;
        mContentView = null;
        mErrorView = null;
    }
}
