package com.github.piasy.app.features.login.di;

import com.github.piasy.app.features.login.LoginPresenterImpl;
import com.github.piasy.app.features.login.mvp.LoginPresenter;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mikhailz on 09/05/16.
 */
@Module
public class LoginModule {

    @Provides
    LoginPresenter provideLoginPresenter(final LoginPresenterImpl loginPresenter) {
        return loginPresenter;
    }

    @Provides
    FacebookProvider provideFacebook(Application application) {
        final FacebookProvider facebookProvider = new FacebookProvider();
        facebookProvider.init(application);
        return facebookProvider;
    }
}
