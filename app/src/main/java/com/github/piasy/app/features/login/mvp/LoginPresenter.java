package com.github.piasy.app.features.login.mvp;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import android.content.Intent;

/**
 * Created by mikhailz on 09/05/16.
 */
public interface LoginPresenter extends MvpPresenter<LoginView> {

    void loginWithUserName(String userName, String password);

    void loginWithFacebook();

    void signupWithUserName(String userName, String password, String email);

    void startSignupWithFacebook();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
