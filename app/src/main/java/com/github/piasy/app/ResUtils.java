package com.github.piasy.app;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by mikhailz on 23/05/2016.
 */
public final class ResUtils {

    private ResUtils() {
    }

    public static float getFloatByDimenId(Context context, int id) {
        TypedValue outValue = new TypedValue();
        context.getResources().getValue(id, outValue, true);
        float value = outValue.getFloat();
        return value;
    }
}
