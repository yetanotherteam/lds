package com.github.piasy.app.features.contentScreen;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mikhailz on 01/06/2016.
 */
@Module
public class ContentModule {

    @Provides
    ContentPresenter provideContentPresenter(final ContentPresenterImpl contentPresenter) {
        return contentPresenter;
    }
}
