package com.github.piasy.app.features.readingScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.features.BaseMvpFragmentLce;
import com.github.piasy.app.features.OnItemClickListener;
import com.github.piasy.app.features.readingScreen.di.ReadingComponent;
import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenter;
import com.github.piasy.model.users.Bookmarks;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 26/05/2016.
 */
public class BookmarksFragment extends BaseMvpFragmentLce<ReadingView, ReadingPresenter,
        ReadingComponent, RecyclerView, Bookmarks> {

    private Bookmarks mData;
    private RecyclerView mRecyclerView;

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = ButterKnife.findById(view, R.id.contentView);

        getPresenter().startLoadingBookmarks();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.bookmarks_list;
    }

    @Override
    public void showContent() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new BookmarkAdapter(mData, new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

            }
        }));
        super.showContent();
    }

    @Override
    public void setData(Bookmarks data) {
        mData = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    private class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookMarksViewHolder> {


        private Bookmarks mData;
        private OnItemClickListener mOnItemClickListener;


        public BookmarkAdapter(Bookmarks data, OnItemClickListener onClickListener) {
            mData = data;
            mOnItemClickListener = onClickListener;
        }


        @Override
        public BookMarksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.bookmark_list_row, parent, false);

            return new BookMarksViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(BookMarksViewHolder holder, int position) {
            holder.getContent().setText(mData.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class BookMarksViewHolder extends RecyclerView.ViewHolder {

            private final TextView mContent;
            private final TextView mRightButton;

            public BookMarksViewHolder(View itemView) {
                super(itemView);
                mContent = ButterKnife.findById(itemView, R.id.tvBookmarkTitle);
                mRightButton = ButterKnife.findById(itemView, R.id.rightButton);
                mRightButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                });
            }

            public TextView getContent() {
                return mContent;
            }

            public TextView getRightButton() {
                return mRightButton;
            }
        }
    }


}
