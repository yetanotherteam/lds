package com.github.piasy.app.features.contentScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.features.BaseMvpFragmentLce;
import com.github.piasy.app.features.OnItemClickListener;
import com.github.piasy.model.dataScheme.Nav_collection;
import com.github.piasy.model.dataScheme.Nav_item;
import com.github.piasy.model.users.GenericList;
import com.yatatsu.autobundle.AutoBundleField;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 01/06/2016.
 */
public class ContentFragment extends BaseMvpFragmentLce<ContentView, ContentPresenter, ContentComponent, RecyclerView, GenericList>
        implements ContentView<GenericList> {

    @AutoBundleField
    String mItemCategoryId;

    private RecyclerView mRecyclerView;
    private GenericList mData;

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContentFragmentAutoBundle.bind(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = ButterKnife.findById(view, R.id.contentView);

        getPresenter().initDatabaseForBookMetaId(mItemCategoryId);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().startSpinnerForCollections();

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.content_list;
    }


    @Override
    public void showContent() {

        Class genericType = mData.getGenericType();

        if (genericType == Nav_item.class) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            ContentAdapter adapter = new ContentAdapter(mData, new OnItemClickListener() {
                @Override
                public void onItemClick(View itemView, int position) {
                    Nav_item nav_item = (Nav_item) mData.get(position);
                    ((ContentActivty)getActivity()).showContentForItem(nav_item);
                }
            });
            mRecyclerView.setAdapter(adapter);
        }

        if (genericType == Nav_collection.class) {
            setSpinner(mData);
        }
        super.showContent();
    }

    @Override
    public void setData(GenericList data) {
        mData = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    private void setSpinner(GenericList list) {


        ArrayAdapter<Nav_collection> adapter = new ArrayAdapter<Nav_collection>(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = ButterKnife.findById(getActivity(), R.id.spChapters);
        spinner.setAdapter(adapter);
        //spinner.setPrompt("Title");
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Nav_collection item = (Nav_collection) parent.getAdapter().getItem(position);
                getPresenter().startContentsForNavCollection(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ContentAdapterViewHolder> {


        private GenericList mData;
        private OnItemClickListener mOnItemClickListener;


        public ContentAdapter(GenericList data, OnItemClickListener onClickListener) {
            mData = data;
            mOnItemClickListener = onClickListener;
        }


        @Override
        public ContentAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.content_list_row, parent, false);

            return new ContentAdapterViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ContentAdapterViewHolder holder, int position) {

            Object o = mData.get(position);
            if (mData.getGenericType() == Nav_item.class) {
                if (o instanceof Nav_item) {
                    Nav_item navItem = (Nav_item) o;
                    holder.getContent().setText(navItem.title());
                }
            }

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class ContentAdapterViewHolder extends RecyclerView.ViewHolder {

            private final TextView mContent;
            private final TextView mRightButton;

            public ContentAdapterViewHolder(View itemView) {
                super(itemView);
                mContent = ButterKnife.findById(itemView, R.id.tvContentTitle);
                mRightButton = ButterKnife.findById(itemView, R.id.rightButton);
                mRightButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                });
            }

            public TextView getContent() {
                return mContent;
            }

            public TextView getRightButton() {
                return mRightButton;
            }
        }
    }
}
