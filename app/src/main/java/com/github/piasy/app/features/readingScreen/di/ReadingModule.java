package com.github.piasy.app.features.readingScreen.di;

import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenter;
import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mikhailz on 26/05/2016.
 */
@Module
public class ReadingModule {

    @Provides
    ReadingPresenter provideReadingPresenter(final ReadingPresenterImpl readingPresenter) {
        return readingPresenter;
    }


}
