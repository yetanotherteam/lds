package com.github.piasy.app.features.contentScreen;

import com.github.piasy.app.IO;
import com.github.piasy.base.model.provider.DatabaseFromArchiveProvider;
import com.github.piasy.base.mvp.NullObjRxBasePresenter;
import com.github.piasy.base.utils.RxDatabaseUtils;
import com.github.piasy.model.dataScheme.Nav_collection;
import com.github.piasy.model.dataScheme.Nav_item;
import com.github.piasy.model.dataScheme.Subitem_content_fts_content;
import com.github.piasy.model.errors.RxNetErrorProcessor;
import com.github.piasy.model.users.BookItemMeta;
import com.github.piasy.model.users.GenericList;
import com.github.piasy.model.users.daoBook.DbBookItemDao;

import android.database.sqlite.SQLiteDatabase;
import android.text.Html;
import android.text.Spanned;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by mikhailz on 01/06/2016.
 */
public class ContentPresenterImpl extends NullObjRxBasePresenter<ContentView> implements ContentPresenter {

    private final RxNetErrorProcessor mRxNetErrorProcessor;
    private DatabaseFromArchiveProvider mDatabaseFromArchiveProvider;
    final DbBookItemDao mBookItemDao;
    private Observable<SQLiteDatabase> databaseObs;

    @Inject
    public ContentPresenterImpl(RxNetErrorProcessor rxNetErrorProcessor,
                                DatabaseFromArchiveProvider databaseFromArchiveProvider,
                                DbBookItemDao bookItemDao) {
        mRxNetErrorProcessor = rxNetErrorProcessor;
        mDatabaseFromArchiveProvider = databaseFromArchiveProvider;
        mBookItemDao = bookItemDao;
    }

    @Override
    public void startContentsForNavCollection(final Nav_collection nav_collection) {

        if (databaseObs == null) {
            throw new IllegalStateException("Init databaseObs first");
        }

        String query =
                "SELECT * FROM nav_item LEFT OUTER JOIN\n" +
                        "(SELECT  ns._id as navSectionId , nc._id as navCollectionId , nc.title as colTitle FROM nav_section ns INNER JOIN nav_collection nc ON nc._id== ns.nav_collection_id ) NS\n" +
                        "ON NS.navSectionId == nav_item.nav_section_id WHERE navCollectionId = ? ";
        Observable<GenericList> listObservable =
                databaseObs

                        .map(RxDatabaseUtils.getCursorFromQuery(query, new String[]{String.valueOf(nav_collection._id())}))
                        .flatMap(RxDatabaseUtils.toCursorObservable())
                        .map(Nav_item.mapper())
                        .toList()
                        .map(new Func1<List<Nav_item>, GenericList>() {
                            @Override
                            public GenericList call(List<Nav_item> nav_items) {
                                return new GenericList<>(Nav_item.class, nav_items);
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread());

        Subscription subscribe = getSubscribe(listObservable);

        addSubscription(subscribe);
    }

    public Subscription getSubscribe(Observable<GenericList> listObservable) {
        return IO.subscribe(
                IO.showAtLceObservable(getView(), listObservable),
                new Action1() {
                    @Override
                    public void call(Object o) {
                    }
                },
                mRxNetErrorProcessor);
    }

    @Override
    public void initDatabaseForBookMetaId(String externalId) {
        databaseObs = mBookItemDao.getBookByExternalId(externalId)
                .flatMap(new Func1<BookItemMeta, Observable<SQLiteDatabase>>() {
                    @Override
                    public Observable<SQLiteDatabase> call(BookItemMeta bookItemMeta) {
                        return mDatabaseFromArchiveProvider.getDatabase(bookItemMeta.url(), "package.sqlite");
                    }
                })
                .first()
                .cache();
    }

    @Override
    public void startSpinnerForCollections() {

        if (databaseObs == null) {
            throw new IllegalStateException("Init databaseObs first");
        }


        Observable<GenericList> genericListObservable = databaseObs
                .map(RxDatabaseUtils.getCursorFromQuery("SELECT * FROM nav_collection", null))
                .flatMap(RxDatabaseUtils.toCursorObservable())
                .map(Nav_collection.mapper())
                .toList()
                .map(new Func1<List<Nav_collection>, GenericList>() {
                    @Override
                    public GenericList call(List<Nav_collection> nav_collection) {
                        return new GenericList<>(Nav_collection.class, nav_collection);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());


        addSubscription(getSubscribe(genericListObservable));
    }

    @Override
    public void startSpinnerForNavItems(Long navSectionId) {
        if (databaseObs == null) {
            throw new IllegalStateException("Init databaseObs first");
        }

        String query = "\n" +
                "SELECT * FROM nav_item LEFT OUTER JOIN \n" +
                "(SELECT  ns._id as navSectionId , nc._id as navCollectionId , " +
                "nc.title as colTitle FROM nav_section ns " +
                "INNER JOIN nav_collection nc ON nc._id== ns.nav_collection_id ) NS \n" +
                "ON NS.navSectionId == nav_item.nav_section_id " +
                "WHERE nav_item.nav_section_id = ?";

        Observable<GenericList> genericListObservable = databaseObs
                .map(RxDatabaseUtils.getCursorFromQuery(query, new String[]{String.valueOf(navSectionId)}))
                .flatMap(RxDatabaseUtils.toCursorObservable())
                .map(Nav_item.mapper())
                .toList()
                .map(new Func1<List<Nav_item>, GenericList>() {
                    @Override
                    public GenericList call(List<Nav_item> nav_items) {
                        return new GenericList<>(Nav_item.class, nav_items);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());


        addSubscription(getSubscribe(genericListObservable));


    }

    public void showHtmlFromNavItem(Nav_item nav_item) {

        String query = "select * from subitem_content_fts_content where c0subitem_id = ?";
        Observable<GenericList> genericListObservable = databaseObs.map(RxDatabaseUtils.getCursorFromQuery(query, new String[]{String.valueOf(nav_item.subitem_id())}))
                .flatMap(RxDatabaseUtils.toCursorObservable())
                .map(Subitem_content_fts_content.mapper())
                .toList()
                .map(new Func1<List<Subitem_content_fts_content>, GenericList>() {
                    @Override
                    public GenericList call(List<Subitem_content_fts_content> subitem_content_fts_contents) {
                        return new GenericList<>(Subitem_content_fts_content.class, subitem_content_fts_contents);
                    }
                });

        addSubscription(getSubscribe(genericListObservable));
    }
}
