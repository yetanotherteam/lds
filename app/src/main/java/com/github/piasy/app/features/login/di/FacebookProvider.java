package com.github.piasy.app.features.login.di;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ogaclejapan.rx.binding.RxEvent;

import org.json.JSONObject;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;

import rx.Observable;
import rx.functions.Func1;

public class FacebookProvider {

    private RxEvent<LoginResult> mLoginResultRxEvent = RxEvent.create();
    private RxEvent<Boolean> mLoginCancleRxEvent = RxEvent.create();
    private RxEvent<FacebookException> mLoginErrorRxEvent = RxEvent.create();
    private RxEvent<Profile> mProfileChangedEvent = RxEvent.create();
    private RxEvent<AccessToken> mAccessTokenRxEvent = RxEvent.create();
    private RxEvent<GraphResponse> mGraphResponseRxEvent = RxEvent.create();
    private ProfileTracker mProfileTracker;
    private AccessTokenTracker mAccessTokenTracker;
    private CallbackManager mCallbackManager;

    public FacebookProvider() {
    }

    public void init(Application application) {

        mCallbackManager = CallbackManager.Factory.create();

        FaceBookLoginListener faceBookLoginListener = new FaceBookLoginListener();

        LoginManager.getInstance().registerCallback(mCallbackManager, faceBookLoginListener);

        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                mProfileChangedEvent.post(currentProfile);
            }
        };

        mAccessTokenTracker = new AccessTokenTracker() {

            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken,
                                                       AccessToken accessToken2) {
                mAccessTokenRxEvent.post(accessToken2);
            }
        };
    }

    public CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    public AccessTokenTracker getAccessTokenTracker() {
        return mAccessTokenTracker;
    }

    public ProfileTracker getProfileTracker() {
        return mProfileTracker;
    }

    public AccessToken getAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    public Profile getProfile() {
        return Profile.getCurrentProfile();
    }

    public boolean isFacebookLogined() {
        return getAccessToken() != null;
    }

    public void stop() {
        mProfileTracker.stopTracking();
        mAccessTokenTracker.stopTracking();
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    public GraphResponse requestMeInfo(AccessToken accessToken, PERMISSIONS... permissions) {

        Iterable<String> strings = Observable.from(permissions)
                .map(new Func1<PERMISSIONS, String>() {
                    @Override
                    public String call(PERMISSIONS permissions) {
                        return permissions.toString();
                    }
                }).toBlocking().toIterable();

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", TextUtils.join(",", strings));
        request.setParameters(parameters);
        return request.executeAndWait();
    }

    public RxEvent<LoginResult> getLoginResultRxEvent() {
        return mLoginResultRxEvent;
    }

    public RxEvent<Boolean> getLoginCancleRxEvent() {
        return mLoginCancleRxEvent;
    }

    public RxEvent<FacebookException> getLoginErrorRxEvent() {
        return mLoginErrorRxEvent;
    }

    public RxEvent<Profile> getProfileChangedEvent() {
        return mProfileChangedEvent;
    }

    public RxEvent<AccessToken> getAccessTokenRxEvent() {
        return mAccessTokenRxEvent;
    }

    public RxEvent<GraphResponse> getGraphResponseRxEvent() {
        return mGraphResponseRxEvent;
    }

    public enum PERMISSIONS {
        ID("id"),
        NAME("name"),
        FIRSTNAME("first_name"),
        LASTNAME("last_name"),
        AGE_RANGE("age_range"),
        LINK("link"),
        GENDER("gender"),
        LOCALE("locale"), TIMEZONE("timezone"),
        UPDATED_TIME("updated_time"),
        VERIFIED("verified"),
        BIRTHDAY("birthday"),
        LOCATION("location"),
        EMAIL("email"),
        PICTURE("picture.width(200).height(200)");

        private String mValue;

        PERMISSIONS(String value) {

            mValue = value;
        }

        public static PERMISSIONS getByString(String string) {
            for (PERMISSIONS permission : values()) {
                if (permission.toString().equals(string)) {
                    return permission;
                }
            }
            return null;
        }


        @Override
        public String toString() {

            return mValue;
        }
    }

    private class FaceBookLoginListener implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            mLoginResultRxEvent.post(loginResult);
        }

        @Override
        public void onCancel() {
            mLoginCancleRxEvent.post(true);
        }

        @Override
        public void onError(FacebookException e) {
            mLoginErrorRxEvent.post(e);
        }
    }
}
