package com.github.piasy.app.features.splash;

import com.commit451.nativestackblur.NativeStackBlur;
import com.github.piasy.app.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mikhailz on 10/05/16.
 */
public final class BlurBackground {

    private static final int RADIUS = 50;
    private static BlurBackground instanse;
    private Bitmap mBitmap;

    public static BlurBackground getInstanse() {
        if (instanse == null) {
            instanse = new BlurBackground();
        }
        return instanse;
    }

    public Bitmap getBitmap(Context context) {
        if (mBitmap == null) {
            mBitmap = calculateBlurredImage(context);
        }
        return mBitmap;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void saveToFile(Bitmap bmp, String filename) {

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap calculateBlurredImage(Context context) {
        Bitmap source = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.milky_way_night_sky);
        return NativeStackBlur.process(source, RADIUS);
    }

}
