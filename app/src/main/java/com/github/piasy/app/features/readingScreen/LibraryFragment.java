package com.github.piasy.app.features.readingScreen;

/**
 * Created by mikhailz on 25/05/2016.
 */

import com.github.piasy.app.R;
import com.github.piasy.app.features.BaseMvpFragmentLce;
import com.github.piasy.app.features.contentScreen.ContentActivtyAutoBundle;
import com.github.piasy.app.features.readingScreen.di.ReadingComponent;
import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenter;
import com.github.piasy.model.catalog.Item;
import com.github.piasy.model.catalog.Item_category;
import com.github.piasy.model.users.GenericList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import butterknife.ButterKnife;


public class LibraryFragment extends BaseMvpFragmentLce<
        ReadingView,
        ReadingPresenter,
        ReadingComponent,
        GridView,
        GenericList> implements ReadingControlActivity.OnBackPressedListener {

    private GridView mGridView;
    private GenericList mData;

    enum State {
        Category, Items
    }

    State mState;

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.libary_fragment_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mGridView = ButterKnife.findById(view, R.id.contentView);

        getPresenter().startCategoryLoading();
    }

    @Override
    public void doBack() {
        switch (mState) {
            case Category:
                break;
            case Items:
                getPresenter().startCategoryLoading();
                break;
        }
    }

    @Override
    public void showContent() {
        mGridView.setAdapter(new BooksAdapter(getActivity(), 0, mData));

        if (mData.getGenericType() == Item_category.class) {
            mState = State.Category;
            ((ReadingControlActivity) getActivity()).setOnBackPressedListener(null);
        }

        if (mData.getGenericType() == Item.class) {
            mState = State.Items;
            ((ReadingControlActivity) getActivity()).setOnBackPressedListener(this);
        }

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Object obj = parent.getAdapter().getItem(position);

                switch (mState) {
                    case Category:
                        Item_category itemCategory = (Item_category) obj;
                        getPresenter().startLoadingItems(itemCategory._id());
                        break;
                    case Items:
                        Item item = (Item) obj;
                        Intent intent = ContentActivtyAutoBundle.createIntentBuilder(item.external_id()).build(getActivity());
                        startActivity(intent);
                        break;
                }
            }
        });
        super.showContent();
    }

    @Override
    public void setData(GenericList data) {
        mData = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    private class BooksAdapter extends ArrayAdapter {
        public BooksAdapter(Context context, int resource, GenericList list) {
            super(context, resource, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;

            if (convertView == null) {
                final LayoutInflater from = LayoutInflater.from(getContext());
                convertView = from.inflate(R.layout.book, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Object obj = getItem(position);
            switch (mState) {
                case Category:
                    if (obj instanceof Item_category) {
                        Item_category item_category = (Item_category) obj;
                        viewHolder.mTvTitle.setText(item_category.name());
                    }
                    break;
                case Items:
                    if (obj instanceof Item) {
                        Item item = (Item) obj;
                        viewHolder.mTvTitle.setText(item.title());
                    }
                    break;
            }

            return convertView;
        }

        class ViewHolder {

            public TextView mTvTitle;

            public ViewHolder(View view) {
                mTvTitle = (TextView) view.findViewById(R.id.tvBookTitle);
            }
        }
    }


}