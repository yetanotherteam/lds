package com.github.piasy.app.features.login;

import com.github.piasy.base.android.BaseDialogFragment;

/**
 * Created by mikhailz on 11/05/16.
 */
public abstract class BaseDialogFragmentFullScreen extends BaseDialogFragment {
    @Override
    protected int getWidth() {
        //for fullscreen
        return 0;
    }

    @Override
    protected int getHeight() {
        //for fullscreen
        return 0;
    }
}
