package com.github.piasy.app.features.readingScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.ResUtils;
import com.github.piasy.app.ViewUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by mikhailz on 23/05/2016.
 */
public class TextViewTitleCheckable extends TextViewCheckable {


    {
        setTextSize(ResUtils.getFloatByDimenId(getContext(), R.dimen.main_menu_unchecked));
        setTextColor(getResources().getColor(android.R.color.white));
        final int margin = (int) ViewUtils.convertDpToPixel(ResUtils.getFloatByDimenId(getContext(),
                R.dimen.main_menu_unchecked_title_margin));
        setMargin(margin);
    }

    public TextViewTitleCheckable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TextViewTitleCheckable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewTitleCheckable(Context context) {
        super(context);
    }

    @Override
    public void setChecked(boolean checked) {
        setTextSize(ResUtils.getFloatByDimenId(getContext(), checked ?
                R.dimen.main_menu_checked : R.dimen.main_menu_unchecked));
        setTextColor(getResources().getColor(checked ? R.color.brown : android.R.color.white));

        final int marginPx = (int) ViewUtils.convertDpToPixel(ResUtils.getFloatByDimenId(getContext(),
                checked ? R.dimen.main_menu_checked_title_margin : R.dimen.main_menu_unchecked_title_margin));

        setMargin(marginPx);
    }

    private void setMargin(int marginPx) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(0,
                marginPx,
                0,
                marginPx);

        setLayoutParams(layoutParams);
    }
}
