package com.github.piasy.app.features.readingScreen;

import com.joanzapata.iconify.widget.IconTextView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;

/**
 * Created by mikhailz on 23/05/2016.
 */
public abstract class TextViewCheckable extends IconTextView implements Checkable {
    private boolean mChecked;

    public TextViewCheckable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TextViewCheckable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewCheckable(Context context) {
        super(context);
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        mChecked = !mChecked;
    }
}
