package com.github.piasy.app.features.login;

import com.github.piasy.app.features.login.di.IAuthProvider;
import com.github.piasy.app.features.login.di.IRequest;
import com.github.piasy.model.errors.RxNetErrorProcessor;
import com.github.piasy.model.users.CurrentUser;
import com.github.piasy.model.users.LoginResponse;
import com.github.piasy.model.users.ServerApi;
import com.github.piasy.model.users.SignUpResponse;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by mikhailz on 19/05/2016.
 */
public class DefaultLoginProvider implements IAuthProvider<SignUpResponse, LoginResponse> {

    private final ServerApi mServerApi;
    private final RxNetErrorProcessor mRxNetErrorProcessor;
    private CurrentUser mCurrentUser;

    @Inject
    public DefaultLoginProvider(final ServerApi serverApi,
                                final RxNetErrorProcessor rxNetErrorProcessor,
                                final CurrentUser currentUser) {
        mServerApi = serverApi;
        mRxNetErrorProcessor = rxNetErrorProcessor;
        mCurrentUser = currentUser;
    }

    @Override
    public Observable<SignUpResponse> signUp(IRequest request) {
        if (request instanceof SignupRequestImpl) {
            SignupRequestImpl signupRequest = (SignupRequestImpl) request;
            return mServerApi.signUpWithEmail(
                    signupRequest.userName(),
                    signupRequest.password(),
                    signupRequest.email(),
                    signupRequest.udid());
        } else {
            return Observable.error(new IllegalArgumentException("Wrong parameter for signup"));
        }
    }

    @Override
    public Observable<LoginResponse> login(IRequest request) {
        if (request instanceof LoginRequestImpl) {
            LoginRequestImpl loginRequest = (LoginRequestImpl) request;
            return mServerApi.loginWithEmail(
                    loginRequest.userName(),
                    loginRequest.password(),
                    "unknown",
                    "android",
                    "1.0");
        } else {
            return Observable.error(new IllegalArgumentException("Wrong parameter for loginWithEmail"));
        }
    }

    @Override
    public boolean isLogined() {
        return mCurrentUser != null;
    }

    @Override
    public void logout() {
        mCurrentUser = null;
    }
}
