package com.github.piasy.app.features.readingScreen.di;

import com.github.piasy.app.features.readingScreen.BookmarksFragment;
import com.github.piasy.app.features.readingScreen.HistoryFragment;
import com.github.piasy.app.features.readingScreen.LibraryFragment;
import com.github.piasy.app.features.readingScreen.ReadingControlActivity;
import com.github.piasy.app.features.readingScreen.ReadingView;
import com.github.piasy.app.features.readingScreen.mvp.ReadingPresenter;
import com.github.piasy.base.di.ActivityModule;
import com.github.piasy.base.di.ActivityScope;
import com.github.piasy.base.di.BaseMvpComponent;
import com.github.piasy.model.users.ProviderDaoModule;

import dagger.Subcomponent;

/**
 * Created by mikhailz on 26/05/2016.
 */
@ActivityScope
@Subcomponent(modules = {ActivityModule.class, ProviderDaoModule.class, ReadingModule.class})
public interface ReadingComponent extends BaseMvpComponent<ReadingView, ReadingPresenter> {

    void inject(ReadingControlActivity readingControlActivity);

    void inject(LibraryFragment libraryFragment);

    void inject(BookmarksFragment bookmarksFragment);

    void inject(HistoryFragment historyFragment);
}
