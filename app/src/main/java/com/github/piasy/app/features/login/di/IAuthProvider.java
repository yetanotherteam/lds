package com.github.piasy.app.features.login.di;

import rx.Observable;

/**
 * Created by mikhailz on 19/05/2016.
 */
public interface IAuthProvider<S, L> {

    Observable<S> signUp(IRequest request);

    Observable<L> login(IRequest request);

    boolean isLogined();

    void logout();
}
