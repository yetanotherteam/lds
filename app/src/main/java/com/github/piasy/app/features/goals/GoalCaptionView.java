package com.github.piasy.app.features.goals;

import com.github.piasy.app.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

/**
 * Created by mikhailz on 30/05/2016.
 */
public class GoalCaptionView extends TextView {
    public GoalCaptionView(Context context) {
        super(context);
    }

    public GoalCaptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GoalCaptionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GoalCaptionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray taTitle = context.obtainStyledAttributes(attrs, R.styleable.Title, 0, 0);
        try {
            String title = taTitle.getString(R.styleable.Title_titleText);
            String label = taTitle.getString(R.styleable.Title_labelText);

            boolean isBig = taTitle.getBoolean(R.styleable.Title_isBig, false);

            setGravity(Gravity.CENTER);
//            float titleSize = ta.getDimension(R.styleable.Title_text, 100f);
//            float labelSize = ta.getDimension(R.styleable.Label_size, 100f);

//            setText(Html.fromHtml(
//                    "<font size=\"48 color=\"#000000\">" + title + "</font>" + "<br />" +
//                            "<font size=\"24 color=\"#e1e0e0\">" + label + "</font>" + "<br />"),
//                    TextView.BufferType.SPANNABLE);

            //is big - one more <big> tag
            setText(Html.fromHtml("<i><big>" + (isBig ? "<big>" : "") + "<font color=\"#FFFFFF\">"
                    + title + "</font>" + (isBig ? "<big>" : "") + "</big></i>" + "<br />"
                    + (isBig ? "<big>" : "") + "<font color=\"#FFFFFF\">" + label + "</font>" + (isBig ? "</big>" : "")));
        } finally {
            taTitle.recycle();
        }
    }

    private void init() {

    }
}
