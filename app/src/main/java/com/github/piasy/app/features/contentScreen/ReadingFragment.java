package com.github.piasy.app.features.contentScreen;

import com.github.piasy.app.R;
import com.github.piasy.app.features.BaseMvpFragmentLce;
import com.github.piasy.model.dataScheme.Nav_collection;
import com.github.piasy.model.dataScheme.Nav_item;
import com.github.piasy.model.dataScheme.Subitem_content_fts_content;
import com.github.piasy.model.users.GenericList;
import com.yatatsu.autobundle.AutoBundleField;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 14/06/2016.
 */
public class ReadingFragment extends BaseMvpFragmentLce<ContentView, ContentPresenter, ContentComponent, RecyclerView, GenericList>
        implements ContentView<GenericList> {


    @AutoBundleField
    Long mNavItemId;

    @AutoBundleField
    Long mSectionId;

    @AutoBundleField
    String mItemCategoryId;

    private GenericList mData;

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReadingFragmentAutoBundle.bind(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getPresenter().initDatabaseForBookMetaId(mItemCategoryId);
    }

    @Override
    public void onResume() {
        super.onResume();

        getPresenter().startSpinnerForNavItems(mSectionId);
    }

    @Override
    public void showContent() {

        Class genericType = mData.getGenericType();

        if (genericType == Nav_item.class) {
            setSpinner(mData);
        }

        if (genericType == Subitem_content_fts_content.class) {

        }
        super.showContent();
    }

    private void setSpinner(GenericList data) {

        ArrayAdapter<Nav_collection> adapter = new ArrayAdapter<Nav_collection>(getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = ButterKnife.findById(getActivity(), R.id.spChapters);
        spinner.setAdapter(adapter);
        //spinner.setPrompt("Title");
        spinner.setSelection(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Nav_item nav_item = (Nav_item) parent.getAdapter().getItem(position);
                getPresenter().showHtmlFromNavItem(nav_item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.reading_content;
    }

    @Override
    public void setData(GenericList data) {
        mData = data;
    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }
}
