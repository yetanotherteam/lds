package com.github.piasy.app.features.readingScreen;

import com.github.piasy.app.BootstrapActivity;
import com.github.piasy.app.BootstrapApp;
import com.github.piasy.app.R;
import com.github.piasy.app.features.readingScreen.di.ReadingComponent;
import com.github.piasy.app.features.readingScreen.di.ReadingModule;
import com.github.piasy.app.features.splash.IconicIcons;
import com.github.piasy.base.di.HasComponent;
import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.IoniconsIcons;

import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 09/05/16.
 */
public class ReadingControlActivity extends BootstrapActivity implements HasComponent<ReadingComponent> {

    private ReadingComponent mReadingComponent;


    protected OnBackPressedListener mOnBackPressedListener;

    public interface OnBackPressedListener {
        void doBack();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final View inflate = View.inflate(this, R.layout.reading_activty, null);
        setContentView(inflate);


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        initToolbar();

    }

    private void initToolbar() {
        Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View bottomSheet = findViewById(R.id.bottom_sheet);
                    assert bottomSheet != null;
                    BottomSheetBehavior<View> mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                    BottomSheetDialogFragment bottomSheetDialogFragment = new MenuBottomSheetFragment();
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());


                }
            });
            final IconDrawable iconDrawable = new IconDrawable(this, FontAwesomeIcons.fa_bars)
                    .colorRes(R.color.brown)
                    .actionBarSize();
            toolbar.setNavigationIcon(iconDrawable);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }

            toolbar.setTitle(R.string.reading_toolbar_title);
        }
    }

    @Override
    protected void initializeInjector() {
        mReadingComponent = BootstrapApp.get()
                .appComponent()
                .plus(getActivityModule(), new ReadingModule());

    }

    @Override
    public ReadingComponent getComponent() {
        return mReadingComponent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);

        menu.findItem(R.id.action_more).setIcon(
                getIcon(IoniconsIcons.ion_android_more_vertical));

        menu.findItem(R.id.action_search).setIcon(
                getIcon(IoniconsIcons.ion_android_search));

        menu.findItem(R.id.action_copy).setIcon(
                getIcon(IconicIcons.gmd_collection_item));

        menu.findItem(R.id.action_backward).setIcon(
                getIcon(IoniconsIcons.ion_ios_undo));

        menu.findItem(R.id.action_forward).setIcon(
                getIcon(IoniconsIcons.ion_ios_redo));

        menu.findItem(R.id.action_bookmarks).setIcon(
                getIcon(IoniconsIcons.ion_ios_bookmarks));

        menu.findItem(R.id.action_history).setIcon(
                getIcon(IoniconsIcons.ion_ios_clock));

        menu.findItem(R.id.action_help).setIcon(
                getIcon(IoniconsIcons.ion_help_circled));

        return true;
    }

    private IconDrawable getIcon(Icon icon) {
        return new IconDrawable(this, icon)
                .colorRes(R.color.brown)
                .actionBarSize();
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LibraryFragment(), "LIBRARY");

        adapter.addFragment(new BookmarksFragment(), "BOOKMARKS");
        adapter.addFragment(new HistoryFragment(), "HISTORY");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public OnBackPressedListener getOnBackPressedListener() {
        return mOnBackPressedListener;
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        mOnBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {
        if (mOnBackPressedListener != null)
            mOnBackPressedListener.doBack();
        else
            super.onBackPressed();
    }
}
