package com.github.piasy.app.features.login;

import com.facebook.Profile;
import com.github.piasy.app.R;
import com.github.piasy.app.features.splash.BlurBackground;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.yatatsu.autobundle.AutoBundleField;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.ButterKnife;


/**
 * Created by mikhailz on 11/05/16.
 */
public class LdsSignupDialogFragment extends BaseDialogFragmentFullScreen implements Validator.ValidationListener {

    @AutoBundleField
    Profile mFacebookProfile;

    @NotEmpty
    @Email
    private AutoCompleteTextView mEdEmail;
    @ConfirmPassword
    private EditText mEdConfirm;
    @Password(scheme = Password.Scheme.ANY)
    private EditText mTvPassword;
    @NotEmpty
    private AutoCompleteTextView mUsername;
    private TextView mTvSignup;
    private OnStartLoginListener mOnStartLoginListener;
    private Validator mValidator;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.signup_lds_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        mEdEmail = ButterKnife.findById(view, R.id.edEmail);
        mTvPassword = ButterKnife.findById(view, R.id.edPassword);
        mTvSignup = ButterKnife.findById(view, R.id.tvSignup);
        mUsername = ButterKnife.findById(view, R.id.edUsername);
        mEdConfirm = ButterKnife.findById(view, R.id.edConfirm);

        ImageView imageView = ButterKnife.findById(view, R.id.ivBackground);
        imageView.setImageBitmap(BlurBackground.getInstanse().getBitmap());

        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int id = v.getId();
                if (id == R.id.tvSignup) {
                    mValidator.validate();
                }
            }
        };

        mTvSignup.setOnClickListener(onClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mFacebookProfile != null) {
            mUsername.setText(mFacebookProfile.getName());
        }
    }

    public void setOnStartActionListener(OnStartLoginListener onStartLoginListener) {
        mOnStartLoginListener = onStartLoginListener;
    }

    @Override
    public void onValidationSucceeded() {
        if (mOnStartLoginListener == null) {
            throw new RuntimeException("Setup OnStartLoginListener");
        }
        mOnStartLoginListener.startSignUp(
                mUsername.getText().toString(),
                mTvPassword.getText().toString(),
                mEdEmail.getText().toString());

        dismiss();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
