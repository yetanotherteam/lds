package com.github.piasy.app.features.readingScreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mikhailz on 23/05/2016.
 */
public class TextViewIconCheckable extends TextViewCheckable {

    {
        setVisibility(GONE);
        setClickable(true);
    }

    public TextViewIconCheckable(Context context) {
        super(context);
    }

    public TextViewIconCheckable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public TextViewIconCheckable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setChecked(final boolean checked) {
        setVisibility(checked ? VISIBLE : GONE);

        animate()
                .translationY(checked ? 0 : getHeight())
                .alpha(checked ? 1.0f : 0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        setVisibility(checked ? VISIBLE : View.GONE);
                    }
                });
    }
}
