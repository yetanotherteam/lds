package com.github.piasy.app.features.login.mvp;

import com.facebook.Profile;
import com.github.piasy.app.features.login.SignupRequestImpl;
import com.github.piasy.model.users.LoginResponse;
import com.github.piasy.model.users.SignUpResponse;
import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by mikhailz on 09/05/16.
 */
public interface LoginView extends MvpView {

    void showStartLogining();

    void showStopLogining();

    void showLoginedState(LoginResponse loginResponse);

    void showCredsErrorState(String msg);

    void showNetworkErrorState();

    void loginWithSignedUpState(SignupRequestImpl request, SignUpResponse signUpResponse);

    void goSignupWindowAndFillProfile(Profile profile);
}
