package com.github.piasy.app.features.contentScreen;

import com.github.piasy.base.di.ActivityModule;
import com.github.piasy.base.di.ActivityScope;
import com.github.piasy.base.di.BaseComponent;
import com.github.piasy.base.di.BaseMvpComponent;
import com.github.piasy.model.users.ProviderDaoModule;

import dagger.Subcomponent;

/**
 * Created by mikhailz on 01/06/2016.
 */
@ActivityScope
@Subcomponent(modules = {ActivityModule.class, ContentModule.class, ProviderDaoModule.class})
public interface ContentComponent extends BaseComponent, BaseMvpComponent<ContentView, ContentPresenter> {

    void inject(ContentFragment contentFragment);

    void inject(ReadingFragment readingFragment);
}
