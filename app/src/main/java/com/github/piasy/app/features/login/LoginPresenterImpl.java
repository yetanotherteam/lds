package com.github.piasy.app.features.login;

import com.facebook.GraphResponse;
import com.github.piasy.app.features.login.di.FacebookProvider;
import com.github.piasy.app.features.login.mvp.LoginPresenter;
import com.github.piasy.app.features.login.mvp.LoginView;
import com.github.piasy.base.mvp.NullObjRxBasePresenter;
import com.github.piasy.model.errors.RxNetErrorProcessor;
import com.github.piasy.model.users.CurrentUser;
import com.github.piasy.model.users.LoginResponse;
import com.github.piasy.model.users.ServerApi;
import com.github.piasy.model.users.SignUpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;

import static com.github.piasy.app.IO.io2main;

/**
 * Created by mikhailz on 09/05/16.
 */
public class LoginPresenterImpl extends NullObjRxBasePresenter<LoginView> implements LoginPresenter {

    private final ServerApi mServerApi;
    private final RxNetErrorProcessor mRxNetErrorProcessor;
    private CurrentUser mCurrentUser;
    private final Action1<LoginResponse> mOnLogin = new Action1<LoginResponse>() {
        @Override
        public void call(LoginResponse loginResponse) {
            mCurrentUser = new CurrentUser(loginResponse);
            getView().showLoginedState(loginResponse);
        }
    };
    private DefaultLoginProvider mDefaultLoginProvider;
    private FacebookProvider mFacebookProvider;

    @Inject
    public LoginPresenterImpl(final ServerApi serverApi,
                              final RxNetErrorProcessor rxNetErrorProcessor,
                              final CurrentUser currentUser,
                              final DefaultLoginProvider defaultLoginProvider,
                              final FacebookProvider facebookProvider) {
        mServerApi = serverApi;
        mRxNetErrorProcessor = rxNetErrorProcessor;
        mCurrentUser = currentUser;
        mDefaultLoginProvider = defaultLoginProvider;
        mFacebookProvider = facebookProvider;
    }

    @Override
    public void loginWithUserName(String userName, String password) {
        final LoginRequestImpl request = LoginRequestImpl.builder()
                .userName(userName)
                .password(password)
                .build();

        addSubscription(withProgressBar(io2main(mDefaultLoginProvider.login(request)))
                .subscribe(mOnLogin, mRxNetErrorProcessor));
    }

    @Override
    public void loginWithFacebook() {

        if (mFacebookProvider.getAccessToken() != null) {

            final Observable<LoginResponse> fbLoginObs = getFbLoginObservable();


            addSubscription(io2main(fbLoginObs)
                    .subscribe(new Action1<LoginResponse>() {
                        @Override
                        public void call(LoginResponse loginResponse) {
                            if (loginResponse.success() == 1) {
                                getView().showLoginedState(loginResponse);
                                mCurrentUser = new CurrentUser(loginResponse);
                            } else {
                                startSignupWithFacebook();
                            }
                        }
                    }, mRxNetErrorProcessor));

        }
    }

    private Observable<LoginResponse> getFbLoginObservable() {
        final Observable<GraphResponse> getMe = getMeObservable();
        return getMe
                .flatMap(new Func1<GraphResponse, Observable<LoginResponse>>() {
                    @Override
                    public Observable<LoginResponse> call(GraphResponse graphResponse) {
                        return getFbLoginObs(graphResponse);
                    }
                });
    }

    @NonNull
    private Observable<GraphResponse> getMeObservable() {
        return Observable.defer(new Func0<Observable<GraphResponse>>() {
            @Override
            public Observable<GraphResponse> call() {
                return getMe();
            }
        });
    }

    private Observable<GraphResponse> getMe() {
        final GraphResponse graphResponse = mFacebookProvider.requestMeInfo(mFacebookProvider.getAccessToken(),
                FacebookProvider.PERMISSIONS.ID,
                FacebookProvider.PERMISSIONS.NAME,
                FacebookProvider.PERMISSIONS.EMAIL,
                FacebookProvider.PERMISSIONS.PICTURE
        );
        return Observable.just(graphResponse);
    }

    private Observable<LoginResponse> getFbLoginObs(GraphResponse graphResponse) {
        final JSONObject jsonObject = graphResponse.getJSONObject();
        try {
            final String email = jsonObject.getString("email");
            final String username = email.split("@")[0];
            final String fbuser = jsonObject.getString("name");

            final String fbid = jsonObject.getString("id");

            String imgurl = null;
            if ((jsonObject.getJSONObject("picture") != null)
                    && jsonObject.getJSONObject("picture").getJSONObject("data") != null) {
                imgurl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
            }
            return mServerApi.loginWithFacebook(
                    email,
                    username,
                    fbid,
                    fbuser,
                    fbuser,
                    imgurl,
                    "android",
                    "1.0"
            );

        } catch (JSONException e) {
            e.printStackTrace();
            return Observable.error(new IllegalArgumentException("Wrong values from fb"));
        }
    }

    private Observable<SignUpResponse> getFbSignUpObs(GraphResponse graphResponse) {
        final JSONObject jsonObject = graphResponse.getJSONObject();
        try {
            final String email = jsonObject.getString("email");
            final String username = email.split("@")[0];
            final String fbuser = jsonObject.getString("name");
            final String fbid = jsonObject.getString("id");

            String imgurl = null;
            if ((jsonObject.getJSONObject("picture") != null)
                    && jsonObject.getJSONObject("picture").getJSONObject("data") != null) {
                imgurl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
            }
            return mServerApi.signUpWithFacebook(
                    email,
                    username,
                    fbid,
                    fbuser,
                    fbuser,
                    imgurl
            );

        } catch (JSONException e) {
            e.printStackTrace();
            return Observable.error(new IllegalArgumentException("Wrong values from fb"));
        }
    }


    @Override
    public void signupWithUserName(String userName, String password, String email) {

        final SignupRequestImpl request = SignupRequestImpl.builder()
                .userName(userName)
                .password(password)
                .email(email)
                .udid("unknown")
                .build();

        addSubscription(withProgressBar(io2main(mDefaultLoginProvider.signUp(request)))
                .subscribe(new Action1<SignUpResponse>() {
                    @Override
                    public void call(SignUpResponse user) {
                        getView().loginWithSignedUpState(request, user);
                    }
                }, mRxNetErrorProcessor));

    }

    @Override
    public void startSignupWithFacebook() {
        if (mFacebookProvider.isFacebookLogined()) {
            addSubscription(
                    io2main(
                            getMeObservable().flatMap(new Func1<GraphResponse, Observable<SignUpResponse>>() {
                                @Override
                                public Observable<SignUpResponse> call(GraphResponse graphResponse) {
                                    return getFbSignUpObs(graphResponse);
                                }
                            }))
                            .subscribe(new Action1<SignUpResponse>() {
                                @Override
                                public void call(SignUpResponse signUpResponse) {
                                    if (signUpResponse.success() == 1) {
                                        loginWithFacebook();
                                    } else {
                                        getView().showCredsErrorState(signUpResponse.msg());
                                    }
                                }
                            }, mRxNetErrorProcessor)
            );
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        mFacebookProvider.getCallbackManager()
                .onActivityResult(requestCode, resultCode, data);

        if (mFacebookProvider.getAccessToken() != null) {
            loginWithFacebook();
        }
    }

    public <T> Observable<T> withProgressBar(Observable<T> observable) {
        return observable
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        getView().showStartLogining();
                    }
                })
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        getView().showStopLogining();
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        getView().showNetworkErrorState();
                    }
                });
    }
}
