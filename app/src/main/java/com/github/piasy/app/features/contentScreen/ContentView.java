package com.github.piasy.app.features.contentScreen;

import com.github.piasy.model.dataScheme.Nav_collection;
import com.github.piasy.model.dataScheme.Nav_section;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

/**
 * Created by mikhailz on 01/06/2016.
 */
public interface ContentView<T> extends MvpLceView<T> {

}
