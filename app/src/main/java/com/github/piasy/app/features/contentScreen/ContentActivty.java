package com.github.piasy.app.features.contentScreen;

import com.github.piasy.app.BootstrapActivity;
import com.github.piasy.app.BootstrapApp;
import com.github.piasy.app.R;
import com.github.piasy.app.features.readingScreen.MenuBottomSheetFragment;
import com.github.piasy.app.features.splash.IconicIcons;
import com.github.piasy.base.di.HasComponent;
import com.github.piasy.model.dataScheme.Nav_item;
import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.IoniconsIcons;
import com.yatatsu.autobundle.AutoBundleField;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 01/06/2016.
 */
public class ContentActivty extends BootstrapActivity implements HasComponent<ContentComponent> {
    private ContentComponent contentComponent;

    @AutoBundleField
    String mExternalCategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ContentActivtyAutoBundle.bind(this, getIntent());

        setContentView(R.layout.content_activity);

        initToolbar();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content,
                        ContentFragmentAutoBundle.createFragmentBuilder(mExternalCategoryId).build())
                .commit();

    }

    public void showContentForItem(Nav_item nav_item) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content,
                        ReadingFragmentAutoBundle.createFragmentBuilder(
                                nav_item._id(),
                                nav_item.nav_section_id(),
                                mExternalCategoryId
                        ).build())
                .addToBackStack(null)
                .commit();
    }


    private void initToolbar() {
        Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View bottomSheet = findViewById(R.id.bottom_sheet);
                    assert bottomSheet != null;
                    BottomSheetBehavior<View> mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                    BottomSheetDialogFragment bottomSheetDialogFragment = new MenuBottomSheetFragment();
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());


                }
            });
            final IconDrawable iconDrawable = new IconDrawable(this, FontAwesomeIcons.fa_bars)
                    .colorRes(R.color.brown)
                    .actionBarSize();
            toolbar.setNavigationIcon(iconDrawable);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
    }

    @Override
    protected void initializeInjector() {
        contentComponent = BootstrapApp.get()
                .appComponent()
                .plus(getActivityModule(), new ContentModule());
    }


    @Override
    public ContentComponent getComponent() {
        return contentComponent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);

        menu.findItem(R.id.action_more).setIcon(
                getIcon(IoniconsIcons.ion_android_more_vertical));

        menu.findItem(R.id.action_search).setIcon(
                getIcon(IoniconsIcons.ion_android_search));

        menu.findItem(R.id.action_copy).setIcon(
                getIcon(IconicIcons.gmd_collection_item));

        menu.findItem(R.id.action_backward).setIcon(
                getIcon(IoniconsIcons.ion_ios_undo));

        menu.findItem(R.id.action_forward).setIcon(
                getIcon(IoniconsIcons.ion_ios_redo));

        menu.findItem(R.id.action_bookmarks).setIcon(
                getIcon(IoniconsIcons.ion_ios_bookmarks));

        menu.findItem(R.id.action_history).setIcon(
                getIcon(IoniconsIcons.ion_ios_clock));

        menu.findItem(R.id.action_help).setIcon(
                getIcon(IoniconsIcons.ion_help_circled));

        return true;
    }

    private IconDrawable getIcon(Icon icon) {
        return new IconDrawable(this, icon)
                .colorRes(R.color.brown)
                .actionBarSize();
    }

    private class ChapterAdapter extends ArrayAdapter<String> {
        public ChapterAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final TextView textView = new TextView(getContext());
            textView.setText(getItem(position));
            return textView;
        }
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }
}
