package com.github.piasy.app.features.contentScreen;

import com.github.piasy.model.dataScheme.Nav_collection;
import com.github.piasy.model.dataScheme.Nav_item;
import com.github.piasy.model.dataScheme.Nav_section;
import com.github.piasy.model.users.BookItemMeta;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

/**
 * Created by mikhailz on 01/06/2016.
 */
public interface ContentPresenter extends MvpPresenter<ContentView> {
    void startContentsForNavCollection(Nav_collection nav_collection);

    void initDatabaseForBookMetaId(String externalId);

    void startSpinnerForCollections();

    void startSpinnerForNavItems(Long navSectionId);

    void showHtmlFromNavItem(Nav_item nav_item);
}
