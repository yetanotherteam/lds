package com.github.piasy.app.features.profile;

import com.github.piasy.base.android.BaseActivity;
import com.github.piasy.model.users.GithubUser;
import com.yatatsu.autobundle.AutoBundleField;

import android.os.Bundle;
import android.widget.Toast;

public class ProfileActivity extends BaseActivity {

    @AutoBundleField
    GithubUser mUser;

    @Override
    protected boolean hasArgs() {
        return true;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this, mUser.login(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void initializeInjector() {

    }
}
