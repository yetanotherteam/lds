package com.github.piasy.app.features.readingScreen;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by mikhailz on 23/05/2016.
 */
public class RadioLinearLayout extends LinearLayout {
    public RadioLinearLayout(Context context) {
        super(context);
    }

    public RadioLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RadioLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RadioLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Determines if given points are inside view
     *
     * @param x    - x coordinate of point
     * @param y    - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    public static boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        //point is inside view bounds
        if ((x > viewX && x < (viewX + view.getWidth())) &&
                (y > viewY && y < (viewY + view.getHeight()))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final CheckableLinearLayout checkableLinearLayout =
                getViewWithXY(this, (int) ev.getRawX(), (int) ev.getRawY());
        if (checkableLinearLayout != null && checkableLinearLayout.isChecked())
            return false;
        setCheckedRecursive(this, false);
        return false;
    }

    private void setCheckedRecursive(ViewGroup parent, boolean checked) {
        int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = parent.getChildAt(i);
            if (v instanceof CheckableLinearLayout) {
                ((CheckableLinearLayout) v).setChecked(checked);
            }

            if (v instanceof ViewGroup) {
                setCheckedRecursive((ViewGroup) v, checked);
            }
        }
    }

    public CheckableLinearLayout getViewWithXY(ViewGroup parent, int x, int y) {

        int count = parent.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = parent.getChildAt(i);
            if (v instanceof CheckableLinearLayout && isPointInsideView(x, y, v))
                return (CheckableLinearLayout) v;

        }
        return null;

    }

}
