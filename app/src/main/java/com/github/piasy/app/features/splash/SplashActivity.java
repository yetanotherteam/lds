/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Piasy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.github.piasy.app.features.splash;

import com.bugtags.library.Bugtags;
import com.bugtags.library.BugtagsOptions;
import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.github.piasy.app.BootstrapActivity;
import com.github.piasy.app.BootstrapApp;
import com.github.piasy.app.BuildConfig;
import com.github.piasy.app.R;
import com.github.piasy.app.analytics.CrashReportingTree;
import com.github.piasy.app.features.login.LoginActivty;
import com.github.piasy.app.features.login.LoginActivtyAutoBundle;
import com.github.piasy.app.features.splash.di.SplashComponent;
import com.github.piasy.base.di.HasComponent;
import com.github.piasy.base.utils.RxUtil;
import com.github.promeg.androidgitsha.lib.GitShaUtils;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.EntypoModule;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.joanzapata.iconify.fonts.MaterialModule;
import com.joanzapata.iconify.fonts.MeteoconsModule;
import com.joanzapata.iconify.fonts.SimpleLineIconsModule;
import com.joanzapata.iconify.fonts.TypiconsModule;
import com.joanzapata.iconify.fonts.WeathericonsModule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import jonathanfinerty.once.Once;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Piasy{github.com/Piasy} on 15/9/19.
 *
 * Splash activity. Init app and handle other Intent action. I imitate the way in
 * <a href="http://frogermcs.github.io/dagger-graph-creation-performance/">frogermcs'  blog:
 * Dagger
 * 2 - graph creation performance</a> to avoid activity state loss.
 */
@SuppressWarnings({
        "PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity",
        "PMD.ModifiedCyclomaticComplexity"
})
public class SplashActivity extends BootstrapActivity implements HasComponent<SplashComponent> {

    private SplashComponent mSplashComponent;
    private View mTvLogin;
    private View mTvSignup;
    private RelativeLayout mButtonPanel;
    private ImageView mIvBackground;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        initialize();

        mTvLogin = ButterKnife.findById(this, R.id.tvLogin);
        mTvSignup = ButterKnife.findById(this, R.id.tvAction);
        mButtonPanel = ButterKnife.findById(this, R.id.buttonPanel);
        mIvBackground = ButterKnife.findById(this, R.id.ivBackground);
        mButtonPanel.setVisibility(View.INVISIBLE);


        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int id = v.getId();
                LoginActivty.Type type;
                if (id == R.id.tvLogin) {
                    type = LoginActivty.Type.Login;
                } else {
                    type = LoginActivty.Type.SignUp;
                }
                final Intent build = LoginActivtyAutoBundle.createIntentBuilder(type)
                        .build(SplashActivity.this);
                startActivity(build);

            }
        };
        mTvLogin.setOnClickListener(onClickListener);
        mTvSignup.setOnClickListener(onClickListener);
    }


    @Override
    protected void initializeInjector() {
        mSplashComponent = BootstrapApp.get().appComponent().plus();
        mSplashComponent.inject(this);
    }

    private void initialize() {
        Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                final BootstrapApp app = BootstrapApp.get();
                if ("release".equals(BuildConfig.BUILD_TYPE)) {
                    Timber.plant(new CrashReportingTree());
                    final BugtagsOptions options =
                            new BugtagsOptions.Builder().trackingLocation(false)
                                    .trackingCrashLog(true)
                                    .trackingConsoleLog(true)
                                    .trackingUserSteps(true)
                                    .build();
                    Bugtags.start("82cdb5f7f8925829ccc4a6e7d5d12216", app,
                            Bugtags.BTGInvocationEventShake, options);
                    Bugtags.setUserData("git_sha", GitShaUtils.getGitSha(app));
                } else {
                    Timber.plant(new Timber.DebugTree());
                }

                Iconify
                        .with(new FontAwesomeModule())
                        .with(new EntypoModule())
                        .with(new TypiconsModule())
                        .with(new MaterialModule())
                        .with(new MaterialCommunityModule())
                        .with(new MeteoconsModule())
                        .with(new WeathericonsModule())
                        .with(new SimpleLineIconsModule())
                        .with(new IoniconsModule())
                        .with(new IconicFontModule());

                Once.initialise(app);
                Fresco.initialize(app);
                FacebookSdk.sdkInitialize(app);

                initFont();

                BlurBackground.getInstanse().getBitmap(SplashActivity.this);
                return Observable.just(true);
            }
        })
                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(final Boolean success) {
                        mButtonPanel.setVisibility(View.VISIBLE);
                        mIvBackground.setImageBitmap(BlurBackground.getInstanse().getBitmap());

                    }
                }, RxUtil.OnErrorLogger);
    }

    private void initFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/BrandonGrotesque-Medium.ttf")
                .setFontAttrId(R.attr.fontPath1)
                .build()
        );
    }

    @Override
    public SplashComponent getComponent() {
        return mSplashComponent;
    }
}
