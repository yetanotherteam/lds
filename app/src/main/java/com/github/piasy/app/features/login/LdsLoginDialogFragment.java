package com.github.piasy.app.features.login;

import com.github.piasy.app.R;
import com.github.piasy.app.features.splash.BlurBackground;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by mikhailz on 11/05/16.
 */
public class LdsLoginDialogFragment extends BaseDialogFragmentFullScreen implements Validator.ValidationListener {

    private Toolbar mToolBar;
    @NotEmpty
    @Email
    private AutoCompleteTextView mTvEmail;
    @Password(scheme = Password.Scheme.ANY)
    private EditText mTvPassword;
    private TextView mTvLogin;
    private OnStartLoginListener mOnStartLoginListener;
    private Validator mValidator;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.login_lds_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);


        mTvEmail = ButterKnife.findById(view, R.id.edEmail);
        mTvPassword = ButterKnife.findById(view, R.id.edPassword);
        mTvLogin = ButterKnife.findById(view, R.id.tvLogin);
//        mToolBar = ButterKnife.findById(view, R.id.toolBar);
//        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back_white);
//        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
//        mToolBar.setTitle(R.string.action_login);

        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int id = v.getId();
                if (id == R.id.tvLogin) {
                    mValidator.validate();
                }
            }
        };

        ImageView imageView = ButterKnife.findById(view, R.id.ivBackground);
        imageView.setImageBitmap(BlurBackground.getInstanse().getBitmap());

        mTvLogin.setOnClickListener(onClickListener);
    }

    public void setOnStartActionListener(OnStartLoginListener onStartLoginListener) {
        mOnStartLoginListener = onStartLoginListener;
    }

    @Override
    public void onValidationSucceeded() {
        if (mOnStartLoginListener == null) {
            throw new RuntimeException("Setup OnStartLoginListener");
        }
        mOnStartLoginListener.startLogin(mTvEmail.getText().toString(), mTvPassword.getText().toString());

        dismiss();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


}
