package com.github.piasy.app.features.login;

import com.google.auto.value.AutoValue;

import com.github.piasy.app.features.login.di.IRequest;

/**
 * Created by mikhailz on 19/05/2016.
 */
@AutoValue
public abstract class SignupRequestImpl implements IRequest {

    public static Builder builder() {
        return new AutoValue_SignupRequestImpl.Builder();
    }

    public abstract String userName();

    public abstract String password();

    public abstract String email();

    public abstract String udid();

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder userName(String userName);

        public abstract Builder password(String password);

        public abstract Builder email(String email);

        public abstract Builder udid(String udid);

        public abstract SignupRequestImpl build();
    }
}
