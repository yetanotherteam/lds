package com.github.piasy.app.features.login;

import com.github.piasy.app.BootstrapActivity;
import com.github.piasy.app.BootstrapApp;
import com.github.piasy.app.features.login.di.LoginComponent;
import com.github.piasy.app.features.login.di.LoginModule;
import com.github.piasy.base.di.HasComponent;
import com.yatatsu.autobundle.AutoBundleField;

import android.os.Bundle;

/**
 * Created by mikhailz on 09/05/16.
 */
public class LoginActivty extends BootstrapActivity implements HasComponent<LoginComponent> {

    @AutoBundleField
    Type mType;
    private LoginComponent mLoginComponent;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final LoginFragment fragment = LoginFragmentAutoBundle.createFragmentBuilder(mType).build();
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment)
                .commit();
    }

    @Override
    protected void initializeInjector() {
        mLoginComponent = BootstrapApp.get().appComponent().plus(getActivityModule(), new LoginModule());
    }

    @Override
    public LoginComponent getComponent() {
        return mLoginComponent;
    }

    @Override
    protected boolean hasArgs() {
        return true;
    }

    public enum Type {Login, SignUp}
}
