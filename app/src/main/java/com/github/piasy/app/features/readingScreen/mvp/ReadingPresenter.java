package com.github.piasy.app.features.readingScreen.mvp;

import com.github.piasy.app.features.readingScreen.ReadingView;
import com.github.piasy.model.users.BookItemMeta;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

/**
 * Created by mikhailz on 26/05/2016.
 */
public interface ReadingPresenter extends MvpPresenter<ReadingView> {
    void initDatabase();

    void startLoadingBookmarks();

    void startCategoryLoading();

    void startLoadingHistory();

    void startLoadingItems(Long categoryId);
}
