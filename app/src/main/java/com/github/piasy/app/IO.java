package com.github.piasy.app;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by mikhailz on 19/05/2016.
 */
public final class IO {

    private IO() {
    }

    public synchronized static <T> Observable<T> io2main(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public synchronized static <T> Observable<T> showAtLceObservable(final MvpLceView<T> view, Observable<T> observable) {
        return io2main(observable)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoading(false);
                    }
                })
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.showError(throwable, false);
                    }
                }).doOnNext(new Action1<T>() {
                    @Override
                    public void call(T t) {
                        view.setData(t);
                    }
                }).doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        view.showContent();
                    }
                });
    }

    public static <T> Subscription subscribe(Observable<T> observable, Action1<T> action,
                                             Action1<Throwable> errorProcessing) {
        return observable.subscribe(action, errorProcessing);
    }

    public static <T> void subscribeLce(MvpLceView<T> view, Observable<T> observable, Action1<T> onFinishAction, Action1<Throwable> errorProcessing) {
        IO.subscribe(
                IO.showAtLceObservable(view, observable), onFinishAction, errorProcessing);
    }
}
