package com.github.piasy.app.features.login;

import com.google.auto.value.AutoValue;

import com.github.piasy.app.features.login.di.IRequest;

/**
 * Created by mikhailz on 19/05/2016.
 */
@AutoValue
public abstract class LoginRequestImpl implements IRequest {

    public static Builder builder() {
        return new AutoValue_LoginRequestImpl.Builder();
    }

    public abstract String userName();

    public abstract String password();

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder userName(String userName);

        public abstract Builder password(String password);

        public abstract LoginRequestImpl build();
    }
}
