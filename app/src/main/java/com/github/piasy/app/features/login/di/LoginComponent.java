package com.github.piasy.app.features.login.di;

import com.github.piasy.app.features.login.LoginFragment;
import com.github.piasy.app.features.login.mvp.LoginPresenter;
import com.github.piasy.app.features.login.mvp.LoginView;
import com.github.piasy.base.di.ActivityModule;
import com.github.piasy.base.di.ActivityScope;
import com.github.piasy.base.di.BaseMvpComponent;
import com.github.piasy.model.users.ProviderDaoModule;

import dagger.Subcomponent;

/**
 * Created by mikhailz on 09/05/16.
 */
@ActivityScope
@Subcomponent(modules = {
        ActivityModule.class, ProviderDaoModule.class, LoginModule.class
})
public interface LoginComponent extends BaseMvpComponent<LoginView, LoginPresenter> {
    void inject(LoginFragment loginFragment);
}
