package com.github.piasy.app.features.goals;

import com.github.piasy.app.BootstrapActivity;
import com.github.piasy.app.R;
import com.github.piasy.app.features.readingScreen.ReadingControlActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.ButterKnife;
import jonathanfinerty.once.Once;

/**
 * Created by mikhailz on 30/05/2016.
 */
public class GoalsActivity extends BootstrapActivity {
    public static final String DONE_TAG = "GOALS";
    private RadioGridGroup mRadioGridGroup;

    @Override
    protected void initializeInjector() {

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.goals_activity);
        initToolbar();

        mRadioGridGroup = ButterKnife.findById(this, R.id.rgGoalsRadioGroup);
        mRadioGridGroup.setOnCheckedChangeListener(new RadioGridGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGridGroup group, int checkedId) {
                ButterKnife.findById(GoalsActivity.this, R.id.btSetGoal).setEnabled(true);
            }
        });
        ButterKnife.findById(this, R.id.btSetGoal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGoal();

                Once.markDone(GoalsActivity.DONE_TAG);

                go();
            }
        });
    }

    private void go() {
        final Intent intent = new Intent(this, ReadingControlActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void setGoal() {
        final int checkedAppCompatRadioButtonId = mRadioGridGroup.getCheckedAppCompatRadioButtonId();

        int goal = 0;
        if (checkedAppCompatRadioButtonId == R.id.rbGoal300) {
            goal = 300;
        } else if (checkedAppCompatRadioButtonId == R.id.rbGoal1000) {
            goal = 1000;
        } else if (checkedAppCompatRadioButtonId == R.id.rbGoal1500) {
            goal = 1500;
        }
        if (checkedAppCompatRadioButtonId == R.id.rbGoal4000) {
            goal = 4000;
        }

        save(goal);
    }

    private void save(int i) {


    }

    private void initToolbar() {
        Toolbar mToolBar = ButterKnife.findById(this, R.id.toolBar);
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back_white);
        mToolBar.setTitle(R.string.title_set_dialy_goal);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
