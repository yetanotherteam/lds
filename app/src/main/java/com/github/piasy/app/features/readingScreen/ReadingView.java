package com.github.piasy.app.features.readingScreen;

import com.github.piasy.model.catalog.Item;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

/**
 * Created by mikhailz on 26/05/2016.
 */
public interface ReadingView<T> extends MvpLceView<T> {
    void showItems(List<Item> items);
}
