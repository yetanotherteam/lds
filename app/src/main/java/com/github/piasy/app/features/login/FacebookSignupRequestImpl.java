package com.github.piasy.app.features.login;

import com.google.auto.value.AutoValue;

import com.github.piasy.app.features.login.di.IRequest;

/**
 * Created by mikhailz on 19/05/2016.
 */
@AutoValue
public abstract class FacebookSignupRequestImpl implements IRequest {

    public static Builder builder() {
        return new AutoValue_FacebookSignupRequestImpl.Builder();
    }

    public abstract SignupRequestImpl signupRequest();

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder signupRequest(SignupRequestImpl signupRequest);

        public abstract FacebookSignupRequestImpl build();
    }
}
