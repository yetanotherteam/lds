package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Library_section {
  public abstract Long _id();
  public abstract String external_id();
  public abstract Long library_collection_id();
  public abstract String library_collection_external_id();
  public abstract Long position();
  public abstract String title();
  public abstract String index_title();

  public static Builder builder() {
    return new AutoValue_Library_section.Builder();
  }

  public static Library_section create(Cursor cursor) {
    return AutoValue_Library_section.createFromCursor(cursor);
  }

  public static Func1<Cursor, Library_section> mapper() {
    return AutoValue_Library_section.MAPPER;
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder external_id(String external_id);

    public abstract Builder library_collection_id(Long library_collection_id);

    public abstract Builder library_collection_external_id(String library_collection_external_id);

    public abstract Builder position(Long position);

    public abstract Builder title(String title);

    public abstract Builder index_title(String index_title);

    public abstract Library_section build();
  }
}
