package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Library_collection {
    public abstract Long _id();

    public abstract String external_id();

    public abstract Long library_section_id();

    public abstract String library_section_external_id();

    public abstract Long position();

    public abstract String title();

    public abstract String cover_renditions();

    public abstract Long type_id();

    public static Builder builder() {
        return new AutoValue_Library_collection.Builder();
    }

    public static Library_collection create(Cursor cursor) {
        return AutoValue_Library_collection.createFromCursor(cursor);
    }

    public static Func1<Cursor, Library_collection> mapper() {
        return AutoValue_Library_collection.MAPPER;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder external_id(String external_id);

        public abstract Builder library_section_id(Long library_section_id);

        public abstract Builder library_section_external_id(String library_section_external_id);

        public abstract Builder position(Long position);

        public abstract Builder title(String title);

        public abstract Builder cover_renditions(String cover_renditions);

        public abstract Builder type_id(Long type_id);

        public abstract Library_collection build();
    }
}
