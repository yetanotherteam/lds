package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Language {
  public abstract Long _id();
  public abstract String lds_language_code();
  public abstract String iso639_3();
  public abstract String bcp47();
  public abstract Long root_library_collection_id();
  public abstract String root_library_collection_external_id();

  public static Language create(Cursor cursor) {
    return AutoValue_Language.createFromCursor(cursor);
  }

  public static Func1<Cursor, Language> mapper() {
    return AutoValue_Language.MAPPER;
  }

  public static Builder builder() {
    return new AutoValue_Language.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder lds_language_code(String lds_language_code);

    public abstract Builder iso639_3(String iso639_3);

    public abstract Builder bcp47(String bcp47);

    public abstract Builder root_library_collection_id(Long root_library_collection_id);

    public abstract Builder root_library_collection_external_id(String root_library_collection_external_id);

    public abstract Language build();
  }
}
