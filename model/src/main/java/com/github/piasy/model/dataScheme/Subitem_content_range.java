package com.github.piasy.model.dataScheme;

public class Subitem_content_range {
  private Long _id;
  private Long subitem_id;
  private String uri;
  private Long start_index;
  private Long end_index;

  public Long get_id() {
    return _id;
  }

  public void set_id(Long _id) {
    this._id = _id;
  }

  public Long getSubitem_id() {
    return subitem_id;
  }

  public void setSubitem_id(Long subitem_id) {
    this.subitem_id = subitem_id;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public Long getStart_index() {
    return start_index;
  }

  public void setStart_index(Long start_index) {
    this.start_index = start_index;
  }

  public Long getEnd_index() {
    return end_index;
  }

  public void setEnd_index(Long end_index) {
    this.end_index = end_index;
  }
}
