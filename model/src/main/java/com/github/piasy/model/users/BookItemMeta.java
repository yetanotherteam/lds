package com.github.piasy.model.users;

import com.google.auto.value.AutoValue;

import com.ryanharter.auto.value.gson.annotations.AutoGson;

import android.support.annotation.Nullable;

/**
 * Created by mikhailz on 06/06/2016.
 */
/*
{
//        "external_id":"_scriptures_nt_000",
//        "latest_version":"14",
//        "url":"http://test.ldsscriptures.com/dbs/224/eng/_scriptures_nt_000_v14.zip",
//        "title":"New Testament",
//        "id":201392132,
//        "short_title":null,
//        "uri":"/scriptures/nt"
//        }
 */
@AutoValue
@AutoGson(AutoValue_BookItemMeta.GsonTypeAdapter.class)
public abstract class BookItemMeta {

    @Nullable
    public abstract String external_id();

    public abstract String latest_version();

    @Nullable
    public abstract String url();

    @Nullable
    public abstract String title();

    public abstract Long id();

    @Nullable
    public abstract String uri();

    @Nullable
    public abstract String short_title();

    public static Builder builder() {
        return new AutoValue_BookItemMeta.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder external_id(String external_id);

        public abstract Builder latest_version(String latest_version);

        public abstract Builder url(String url);

        public abstract Builder title(String title);

        public abstract Builder id(Long id);

        public abstract Builder uri(String uri);

        public abstract Builder short_title(String short_title);

        public abstract BookItemMeta build();
    }
}
