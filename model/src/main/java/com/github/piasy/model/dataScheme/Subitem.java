package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;
import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Subitem {
    public abstract Long _id();
    public abstract String uri();
    public abstract Long position();
    public abstract String title();
    public abstract String short_title();
    public abstract String primary_title_component();
    public abstract String secondary_title_component();
    public abstract String web_url();
    public abstract String doc_version();

    public static Builder builder() {
        return new AutoValue_Subitem.Builder();
    }


    public static Subitem create(Cursor cursor) {
        return AutoValue_Subitem.createFromCursor(cursor);
    }

    // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
    public static Func1<Cursor, Subitem> mapper() {
        return AutoValue_Subitem.MAPPER;
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder uri(String uri);

        public abstract Builder position(Long position);

        public abstract Builder title(String title);

        public abstract Builder short_title(String short_title);

        public abstract Builder primary_title_component(String primary_title_component);

        public abstract Builder secondary_title_component(String secondary_title_component);

        public abstract Builder web_url(String web_url);

        public abstract Builder doc_version(String doc_version);

        public abstract Subitem build();
    }
}
