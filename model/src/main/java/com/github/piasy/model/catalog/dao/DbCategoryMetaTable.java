package com.github.piasy.model.catalog.dao;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by mikhailz on 12/06/2016.
 */
public class DbCategoryMetaTable {
    public static final String QUERY_ALL_CATEGORIES = "SELECT * FROM item_category";

    public static final String QUERY_ALL_ITEMS_IN_CATEGORY =
            "SELECT * FROM item c INNER JOIN item_category i ON c.item_category_id == i._id WHERE c.item_category_id == ?";

    public static final String librarySections= "" +
            "select lc.* from library_collection lc, library_section ls " +
            "where ls.library_collection_id= ? and not(ls.title='Support') " +
            "and lc.library_section_id=ls._id order by ls.position,lc.position";

    public static final String libraryCollections = "SELECT * FROM library_collection WHERE library_section_id = ? ORDER BY position";

    public static final String groupSectionsForCollection = "select li.* from library_section ls, library_item li where ls.library_collection_id= ? and ls._id=li.library_section_id order by ls.position,li.position";

    public static final String itemsForSection = "SELECT * FROM library_item WHERE library_section_id=? ORDER BY position";
}
