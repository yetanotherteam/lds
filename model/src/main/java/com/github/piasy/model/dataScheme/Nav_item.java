package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import javax.annotation.Nullable;

import rx.functions.Func1;

@AutoValue
public abstract class Nav_item {
  @Nullable
  public abstract Long _id();
  @Nullable
  public abstract Long nav_section_id();
  @Nullable
  public abstract Long position();
  @Nullable
  public abstract String title();
  @Nullable
  public abstract String short_title();
  @Nullable
  public abstract String primary_title_component();
  @Nullable
  public abstract String secondary_title_component();
  @Nullable
  public abstract String preview();
  @Nullable
  public abstract String uri();
  @Nullable
  public abstract Long subitem_id();

  public static Builder builder() {
    return new AutoValue_Nav_item.Builder();
  }


  public static Nav_item create(Cursor cursor) {
    return AutoValue_Nav_item.createFromCursor(cursor);
  }

  // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
  public static Func1<Cursor, Nav_item> mapper() {
    return AutoValue_Nav_item.MAPPER;
  }


  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder nav_section_id(Long nav_section_id);

    public abstract Builder position(Long position);

    public abstract Builder title(String title);

    public abstract Builder short_title(String short_title);

    public abstract Builder primary_title_component(String primary_title_component);

    public abstract Builder secondary_title_component(String secondary_title_component);

    public abstract Builder preview(String preview);

    public abstract Builder uri(String uri);

    public abstract Builder subitem_id(Long subitem_id);

    public abstract Nav_item build();
  }

  @Override
  public String toString() {
    return title();
  }
}
