package com.github.piasy.model.users;

/**
 * Created by mikhailz on 17/05/16.
 */
public class CurrentUser {
    private final LoginResponse mLoginResponse;

    public CurrentUser(final LoginResponse loginResponse) {
        mLoginResponse = loginResponse;
    }

    public LoginResponse getLoginResponse() {
        return mLoginResponse;
    }

    public boolean isLogined() {
        return mLoginResponse != null;
    }
}
