package com.github.piasy.model.users;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikhailz on 12/06/2016.
 */
public class GenericList<T> extends ArrayList<T> {
    private Class<T> genericType;


    public GenericList(Class<T> c) {
        this.genericType = c;
    }

    public GenericList(Class<T> c,List<T> list) {
        this.genericType = c;
        addAll(list);
    }

    public Class<T> getGenericType() {
        return genericType;
    }
}