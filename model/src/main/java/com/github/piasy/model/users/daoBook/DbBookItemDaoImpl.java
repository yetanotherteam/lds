package com.github.piasy.model.users.daoBook;

import com.github.piasy.base.model.provider.AccessPreferences;
import com.github.piasy.model.users.BookItemMeta;
import com.github.piasy.model.users.BookMeta;
import com.github.piasy.model.users.DbVersionInfo;
import com.github.piasy.model.users.ServerApi;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class DbBookItemDaoImpl implements DbBookItemDao {

    private final DbBookItemMetaDelegate mDbBookItemMetaDelegate;
    private final ServerApi mServerApi;
    private AccessPreferences mAccessPreferences;

    @Inject
    public DbBookItemDaoImpl(final DbBookItemMetaDelegate dbBookItemMetaDelegate, final ServerApi serverApi, AccessPreferences accessPreferences) {
        mDbBookItemMetaDelegate = dbBookItemMetaDelegate;
        mServerApi = serverApi;
        mAccessPreferences = accessPreferences;
    }

    @NonNull
    @Override
    public Observable<List<BookItemMeta>> getAllBooks() {
        return getBookItemMetaObservable()
                .toList()
                .retry(3)
                .onErrorReturn(new Func1<Throwable, List<BookItemMeta>>() {
                    @Override
                    public List<BookItemMeta> call(Throwable throwable) {
                        return mDbBookItemMetaDelegate.getAllBookItemMeta();
                    }
                });
    }

    private Observable<BookItemMeta> getBookItemMetaObservable() {
        return getBooksMeta().flatMap(new Func1<BookMeta, Observable<BookItemMeta>>() {
            @Override
            public Observable<BookItemMeta> call(BookMeta bookMeta) {
                return Observable.from(bookMeta.item());
            }
        });
    }

    @Override
    public Observable<BookMeta> getBooksMeta() {
        return Observable.defer(new Func0<Observable<BookMeta>>() {
            @Override
            public Observable<BookMeta> call() {
                return mServerApi.getBooksMeta().cache();
            }
        });
    }

    @NonNull
    @Override
    public Observable<BookItemMeta> getBookById(final long id) {
        return Observable.defer(new Func0<Observable<BookItemMeta>>() {
            @Override
            public Observable<BookItemMeta> call() {
                return getBookItemMetaObservable().filter(new Func1<BookItemMeta, Boolean>() {
                    @Override
                    public Boolean call(BookItemMeta bookItemMeta) {
                        return bookItemMeta.id() == id;
                    }
                });
            }
        });
    }

    @NonNull
    @Override
    public Observable<BookItemMeta> getBookByExternalId(final String externalId) {
        return Observable.defer(new Func0<Observable<BookItemMeta>>() {
            @Override
            public Observable<BookItemMeta> call() {
                return getBookItemMetaObservable().filter(new Func1<BookItemMeta, Boolean>() {
                    @Override
                    public Boolean call(BookItemMeta bookItemMeta) {
                        return externalId.equals(bookItemMeta.external_id());
                    }
                });
            }
        });
    }


    @NonNull
    @Override
    public Observable<Boolean> isNewDbVersion() {
        return Observable.zip(
                Observable.just(
                        mAccessPreferences.get(AccessPreferences.KEY.DB_VERSION.name(), 0)),
                mServerApi.getDbVersionInfo(),
                new Func2<Integer, DbVersionInfo, Boolean>() {
                    @Override
                    public Boolean call(Integer integer, DbVersionInfo dbVersionInfo) {
                        return dbVersionInfo.dbversion() != integer.intValue();
                    }
                });
    }

    @NonNull
    @Override
    public void saveToDb(List<BookItemMeta> bookItemMetaList) {
        mDbBookItemMetaDelegate.putBookItemMeta(bookItemMetaList);
    }

}
