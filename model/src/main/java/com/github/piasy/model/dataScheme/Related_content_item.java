package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import javax.annotation.Nullable;

import rx.functions.Func1;

@AutoValue
public abstract class Related_content_item {
    public abstract Long _id();

    @Nullable
    public abstract Long subitem_id();

    @Nullable
    public abstract Long position();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String label();

    @Nullable
    public abstract String label_content();

    @Nullable
    public abstract String origin_uri();

    @Nullable
    public abstract String content();

    public static Builder builder() {
        return new AutoValue_Related_content_item.Builder();
    }

    public static Related_content_item create(Cursor cursor) {
        return AutoValue_Related_content_item.createFromCursor(cursor);
    }

    // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
    public static Func1<Cursor, Related_content_item> mapper() {
        return AutoValue_Related_content_item.MAPPER;
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder subitem_id(Long subitem_id);

        public abstract Builder position(Long position);

        public abstract Builder name(String name);

        public abstract Builder label(String label);

        public abstract Builder label_content(String label_content);

        public abstract Builder origin_uri(String origin_uri);

        public abstract Builder content(String content);

        public abstract Related_content_item build();
    }
}
