package com.github.piasy.model.users;

import com.google.auto.value.AutoValue;

import com.ryanharter.auto.value.gson.annotations.AutoGson;

import android.support.annotation.Nullable;

/**
 * Created by mikhailz on 06/06/2016.
 */
@AutoValue
@AutoGson(AutoValue_DbVersionInfo.GsonTypeAdapter.class)
public abstract class DbVersionInfo {

    public abstract int dbversion();

    public static Builder builder() {
        return new AutoValue_DbVersionInfo.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder dbversion(int dbversion);

        public abstract DbVersionInfo build();
    }
}
