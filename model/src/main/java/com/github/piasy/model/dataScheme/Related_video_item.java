package com.github.piasy.model.dataScheme;

public class Related_video_item {
  private Long _id;
  private Long subitem_id;
  private String media_id;
  private String media_url;
  private Long container_type;
  private Long encoding_rate_bps;
  private Long width;
  private Long height;
  private String still_hash;

  public Long get_id() {
    return _id;
  }

  public void set_id(Long _id) {
    this._id = _id;
  }

  public Long getSubitem_id() {
    return subitem_id;
  }

  public void setSubitem_id(Long subitem_id) {
    this.subitem_id = subitem_id;
  }

  public String getMedia_id() {
    return media_id;
  }

  public void setMedia_id(String media_id) {
    this.media_id = media_id;
  }

  public String getMedia_url() {
    return media_url;
  }

  public void setMedia_url(String media_url) {
    this.media_url = media_url;
  }

  public Long getContainer_type() {
    return container_type;
  }

  public void setContainer_type(Long container_type) {
    this.container_type = container_type;
  }

  public Long getEncoding_rate_bps() {
    return encoding_rate_bps;
  }

  public void setEncoding_rate_bps(Long encoding_rate_bps) {
    this.encoding_rate_bps = encoding_rate_bps;
  }

  public Long getWidth() {
    return width;
  }

  public void setWidth(Long width) {
    this.width = width;
  }

  public Long getHeight() {
    return height;
  }

  public void setHeight(Long height) {
    this.height = height;
  }

  public String getStill_hash() {
    return still_hash;
  }

  public void setStill_hash(String still_hash) {
    this.still_hash = still_hash;
  }
}
