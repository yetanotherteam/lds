package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import javax.annotation.Nullable;

import rx.functions.Func1;

@AutoValue
public abstract class Related_audio_item {
  public abstract Long _id();
  @Nullable
  public abstract Long subitem_id();
  @Nullable
  public abstract String media_id();
  @Nullable
  public abstract String media_url();
  @Nullable
  public abstract Long file_size();

  public static Builder builder() {
    return new AutoValue_Related_audio_item.Builder();
  }

  public static Related_audio_item create(Cursor cursor) {
    return AutoValue_Related_audio_item.createFromCursor(cursor);
  }

  // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
  public static Func1<Cursor, Related_audio_item> mapper() {
    return AutoValue_Related_audio_item.MAPPER;
  }


  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder subitem_id(Long subitem_id);

    public abstract Builder media_id(String media_id);

    public abstract Builder media_url(String media_url);

    public abstract Builder file_size(Long file_size);

    public abstract Related_audio_item build();
  }
}
