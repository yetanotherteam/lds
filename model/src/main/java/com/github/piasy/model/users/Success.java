package com.github.piasy.model.users;

/**
 * Created by mikhailz on 20/05/2016.
 */
public enum Success {

    TRUE(1), FALSE(0);

    private int mI;

    Success(int i) {
        mI = i;
    }

    public int getValue() {
        return mI;
    }
}
