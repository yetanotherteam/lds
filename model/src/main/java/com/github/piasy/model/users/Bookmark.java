package com.github.piasy.model.users;

/**
 * Created by mikhailz on 26/05/2016.
 */
public class Bookmark {

    private String mTitle;

    public Bookmark(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
