package com.github.piasy.model.users;

import com.google.auto.value.AutoValue;

import com.ryanharter.auto.value.gson.annotations.AutoGson;

import android.os.Parcelable;
import android.support.annotation.Nullable;

/**
 * Created by mikhailz on 17/05/16.
 */
@AutoValue
@AutoGson(AutoValue_SignUpResponse.GsonTypeAdapter.class)
public abstract class SignUpResponse implements Parcelable {

    @Nullable
    public abstract String imgurl();

    public abstract String userid();

    @Nullable
    public abstract String fbuser();

    public abstract int success();

    @Nullable
    public abstract String msg();

    @Nullable
    public abstract String fbid();

    @Nullable
    public abstract String username();

    public static Builder builder() {
        return new AutoValue_SignUpResponse.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder imgurl(String imgurl);

        public abstract Builder userid(String userid);

        public abstract Builder fbuser(String fbuser);

        public abstract Builder success(int success);

        public abstract Builder msg(String msg);

        public abstract Builder fbid(String fbid);

        public abstract Builder username(String username);

        public abstract SignUpResponse build();
    }
}
