package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Source {
  public abstract Long _id();
  public abstract String name();
  public abstract Long type_id();

  public static Builder builder() {
    return new AutoValue_Source.Builder();
  }

  public static Source create(Cursor cursor) {
    return AutoValue_Source.createFromCursor(cursor);
  }

  public static Func1<Cursor, Source> mapper() {
    return AutoValue_Source.MAPPER;
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder name(String name);

    public abstract Builder type_id(Long type_id);

    public abstract Source build();
  }
}
