package com.github.piasy.model.users.dao;

import android.support.annotation.NonNull;
import com.github.piasy.base.di.ActivityScope;
import com.github.piasy.model.users.GithubApi;
import com.github.piasy.model.users.GithubUser;
import com.github.piasy.model.users.GithubUserSearchResult;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by mikhailz on 19/05/2016.
 */
@ActivityScope
public final class GithubUserDaoImpl implements GithubUserDao {

    private final DbUserDelegate mDbUserDelegate;
    private final GithubApi mGithubApi;

    @Inject
    public GithubUserDaoImpl(final DbUserDelegate dbUserDelegate, final GithubApi githubApi) {
        mDbUserDelegate = dbUserDelegate;
        mGithubApi = githubApi;
    }

    @NonNull
    @Override
    public Observable<List<GithubUser>> searchUser(@NonNull final String query) {
        return mGithubApi.searchGithubUsers(query, GithubApi.GITHUB_API_PARAMS_SEARCH_SORT_JOINED,
                GithubApi.GITHUB_API_PARAMS_SEARCH_ORDER_DESC)
                .map(new Func1<GithubUserSearchResult, List<GithubUser>>() {
                    @Override
                    public List<GithubUser> call(
                            final GithubUserSearchResult githubUserSearchResult) {
                        return githubUserSearchResult.items();
                    }
                })
                .doOnNext(new Action1<List<GithubUser>>() {
                    @Override
                    public void call(final List<GithubUser> githubUsers) {
                        mDbUserDelegate.putAllGithubUser(githubUsers);
                    }
                });
    }
}
