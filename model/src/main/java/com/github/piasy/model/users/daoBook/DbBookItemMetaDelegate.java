package com.github.piasy.model.users.daoBook;

import com.github.piasy.model.users.BookItemMeta;
import com.github.piasy.model.users.GithubUser;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import java.util.List;

import rx.Observable;

/**
 * Created by mikhailz on 06/06/2016.
 */
public interface DbBookItemMetaDelegate {

    DeleteResult deleteAllBooksMeta();

    PutResults<BookItemMeta> putBookItemMeta(List<BookItemMeta> bookItemMetaList);

    List<BookItemMeta> getAllBookItemMeta();

    Observable<List<BookItemMeta>> getAllMetListObservable();

    Observable<BookItemMeta> getBookById(long id);
}
