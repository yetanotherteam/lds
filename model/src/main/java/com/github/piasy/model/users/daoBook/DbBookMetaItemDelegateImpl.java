package com.github.piasy.model.users.daoBook;

import com.github.piasy.model.users.BookItemMeta;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class DbBookMetaItemDelegateImpl implements DbBookItemMetaDelegate {

    private final StorIOSQLite mStorIOSQLite;

    @Inject
    public DbBookMetaItemDelegateImpl(final StorIOSQLite mStorIOSQLite) {
        this.mStorIOSQLite = mStorIOSQLite;
    }

    @Override
    public DeleteResult deleteAllBooksMeta() {
        return mStorIOSQLite.delete()
                .byQuery(DbBookMetaTable.getDeleteAllQuery())
                .prepare()
                .executeAsBlocking();
    }

    @Override
    public PutResults<BookItemMeta> putBookItemMeta(List<BookItemMeta> bookItemMetaList) {
        return mStorIOSQLite.put().objects(bookItemMetaList)
                .prepare().executeAsBlocking();
    }

    @Override
    public List<BookItemMeta> getAllBookItemMeta() {
        return mStorIOSQLite.get()
                .listOfObjects(BookItemMeta.class)
                .withQuery(DbBookMetaTable.QUERY_ALL)
                .prepare()
                .executeAsBlocking();
    }

    @Override
    public Observable<List<BookItemMeta>> getAllMetListObservable() {
        return mStorIOSQLite.get()
                .listOfObjects(BookItemMeta.class)
                .withQuery(DbBookMetaTable.QUERY_ALL)
                .prepare()
                .asRxObservable()
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<BookItemMeta> getBookById(long id) {
        Query GET_BY_ID = Query.builder().table(DbBookMetaTable.TABLE).where("id = ?").whereArgs(id).build();
        return mStorIOSQLite.get()
                .object(BookItemMeta.class)
                .withQuery(GET_BY_ID)
                .prepare()
                .asRxObservable()
                .subscribeOn(Schedulers.io());



    }
}
