package com.github.piasy.model.users.daoBook;

import com.github.piasy.model.users.BookItemMeta;
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver;
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio.sqlite.operations.get.GetResolver;
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.UpdateQuery;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

/**
 * Created by mikhailz on 06/06/2016.
 */
public class DbBookMetaTable {

    @NonNull
    public static final String TABLE = "BookMeta";

    @NonNull
    public static final String COLUMN_EXTERNAL_ID = "COLUMN_EXTERNAL_ID";

    @NonNull
    public static final String COLUMN_LATEST_VERSION = "COLUMN_LATEST_VERSION";

    @NonNull
    public static final String COLUMN_URL = "COLUMN_URL";

    @NonNull
    public static final String COLUMN_TITLE = "COLUMN_TITLE";

    @NonNull
    public static final String COLUMN_ID = "COLUMN_ID";

    @NonNull
    public static final String COLUMN_SHORT_TITLE = "COLUMN_SHORT_TITLE";

    @NonNull
    public static final String COLUMN_URI = "COLUMN_URI";

    @NonNull
    public static final Query QUERY_ALL = Query.builder().table(TABLE).build();



    @NonNull
    public static final PutResolver<BookItemMeta> BOOK_ITEM_META_PUT_RESOLVER =
            new DefaultPutResolver<BookItemMeta>() {
                @NonNull
                @Override
                protected InsertQuery mapToInsertQuery(@NonNull BookItemMeta object) {
                    return InsertQuery.builder().table(TABLE).build();
                }

                @NonNull
                @Override
                protected UpdateQuery mapToUpdateQuery(@NonNull BookItemMeta object) {
                    return UpdateQuery.builder()
                            .table(TABLE)
                            .where(COLUMN_ID + " = ?")
                            .whereArgs(object.id())
                            .build();
                }

                @NonNull
                @Override
                protected ContentValues mapToContentValues(@NonNull BookItemMeta object) {
                    final ContentValues contentValues = new ContentValues(8);

                    contentValues.put(COLUMN_EXTERNAL_ID, object.external_id());
                    contentValues.put(COLUMN_LATEST_VERSION, object.latest_version());
                    contentValues.put(COLUMN_URL, object.url());
                    contentValues.put(COLUMN_TITLE, object.title());

                    contentValues.put(COLUMN_ID, object.id());
                    contentValues.put(COLUMN_SHORT_TITLE, object.short_title());
                    contentValues.put(COLUMN_URI, object.uri());
                    return contentValues;
                }
            };

    @NonNull
    public static final GetResolver<BookItemMeta> BOOK_ITEM_META_GET_RESOLVER =
            new DefaultGetResolver<BookItemMeta>() {
                @NonNull
                @Override
                public BookItemMeta mapFromCursor(@NonNull Cursor cursor) {
                    return BookItemMeta.builder()
                            .external_id(cursor.getString(cursor.getColumnIndex(COLUMN_EXTERNAL_ID)))
                            .latest_version(cursor.getString(cursor.getColumnIndex(COLUMN_LATEST_VERSION)))
                            .url(cursor.getString(cursor.getColumnIndex(COLUMN_URL)))
                            .title(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)))
                            .short_title(cursor.getString(cursor.getColumnIndex(COLUMN_SHORT_TITLE)))
                            .uri(cursor.getString(cursor.getColumnIndex(COLUMN_URI)))
                            .build();
                }
            };

    @NonNull
    public static final DeleteResolver<BookItemMeta> BOOK_ITEM_META_DELETE_RESOLVER =
            new DefaultDeleteResolver<BookItemMeta>() {
                @NonNull
                @Override
                public DeleteQuery mapToDeleteQuery(@NonNull BookItemMeta object) {
                    return DeleteQuery.builder()
                            .table(TABLE)
                            .where(COLUMN_ID + " = ?")
                            .whereArgs(object.id())
                            .build();
                }
            };

    // This is just class with Meta Data, we don't need instances
    private DbBookMetaTable() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateBookMetaTableSQL() {
        return CreateBookMetaTableSQLHolder.CREATE_GITHUB_USER_TABLE_SQL;
    }

    public static DeleteQuery getDeleteAllQuery() {
        return DeleteAllQueryHolder.DELETE_ALL_QUERY;
    }

    private static class CreateBookMetaTableSQLHolder {
        // lazy instantiate
        private static final String CREATE_GITHUB_USER_TABLE_SQL = "CREATE TABLE " + TABLE + "(" +
                COLUMN_EXTERNAL_ID + " TEXT , " +
                COLUMN_LATEST_VERSION + " TEXT , " +
                COLUMN_ID + " INTEGER, " +
                COLUMN_TITLE + " TEXT , " +
                COLUMN_SHORT_TITLE + " TEXT, " +
                COLUMN_URI + " TEXT , " +
                COLUMN_URL + " TEXT " +
                ");";
    }

    private static class DeleteAllQueryHolder {
        // lazy instantiate
        private static final DeleteQuery DELETE_ALL_QUERY =
                DeleteQuery.builder().table(TABLE).build();
    }

}
