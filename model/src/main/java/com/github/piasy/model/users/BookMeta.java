package com.github.piasy.model.users;

import com.google.auto.value.AutoValue;

import com.ryanharter.auto.value.gson.annotations.AutoGson;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by mikhailz on 06/06/2016.
 */

//{
//        "citations_url":"http://test.ldsscriptures.com/dbs/224/eng/citations.sqlite",
//        "language_id":"1",
//        "language":"eng",
//        "catalog_url":"http://test.ldsscriptures.com/dbs/224/catalog224.zip",
//        "catalog":"224",
//        "item":[
//        {
//        "external_id":"_scriptures_ot_000",
//        "latest_version":"14",
//        "url":"http://test.ldsscriptures.com/dbs/224/eng/_scriptures_ot_000_v14.zip",
//        "title":"Old Testament",
//        "id":201392131,
//        "short_title":null,
//        "uri":"/scriptures/ot"
//        },
//        {
//        "external_id":"_scriptures_nt_000",
//        "latest_version":"14",
//        "url":"http://test.ldsscriptures.com/dbs/224/eng/_scriptures_nt_000_v14.zip",
//        "title":"New Testament",
//        "id":201392132,
//        "short_title":null,
//        "uri":"/scriptures/nt"
//        }
@AutoValue
@AutoGson(AutoValue_BookMeta.GsonTypeAdapter.class)
public abstract class BookMeta {

    @Nullable
    public abstract String citations_url();

    public abstract int language_id();

    @Nullable
    public abstract String language();

    @Nullable
    public abstract String catalog_url();

    public abstract int catalog();

    public abstract BookItemMetaList item();

    public abstract int root_library_collection_id();

    @Nullable
    public abstract String size();

    public static Builder builder() {
        return new AutoValue_BookMeta.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder citations_url(String citations_url);

        public abstract Builder language_id(int language_id);

        public abstract Builder language(String language);

        public abstract Builder catalog_url(String catalog_url);

        public abstract Builder catalog(int catalog);

        public abstract Builder item(BookItemMetaList item);

        public abstract Builder root_library_collection_id(int root_library_collection_id);

        public abstract Builder size(String size);

        public abstract BookMeta build();
    }
}
