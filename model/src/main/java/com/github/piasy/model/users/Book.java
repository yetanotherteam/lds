package com.github.piasy.model.users;

/**
 * Created by mikhailz on 25/05/2016.
 */
public class Book {

    private String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }
}
