package com.github.piasy.model.dataScheme;

public class Sqlite_sequence {
  private String name;
  private String seq;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSeq() {
    return seq;
  }

  public void setSeq(String seq) {
    this.seq = seq;
  }
}
