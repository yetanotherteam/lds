package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import javax.annotation.Nullable;

import rx.functions.Func1;

@AutoValue
public abstract class Item {

    public abstract Long _id();

    @Nullable
    public abstract String external_id();

    @Nullable
    public abstract Long language_id();

    @Nullable
    public abstract Long source_id();

    @Nullable
    public abstract String uri();

    @Nullable
    public abstract String title();

    @Nullable
    public abstract String short_title();

    @Nullable
    public abstract String primary_title_component();

    @Nullable
    public abstract String secondary_title_component();

    @Nullable
    public abstract String item_cover_renditions();

    @Nullable
    public abstract Long item_category_id();

    @Nullable
    public abstract Long latest_version();

    @Nullable
    public abstract Long is_obsolete();

    public static Builder builder() {
        return new AutoValue_Item.Builder();
    }

    public static Item create(Cursor cursor) {
        return AutoValue_Item.createFromCursor(cursor);
    }

    // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
    public static Func1<Cursor, Item> mapper() {
        return AutoValue_Item.MAPPER;
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder external_id(String external_id);

        public abstract Builder language_id(Long language_id);

        public abstract Builder source_id(Long source_id);

        public abstract Builder uri(String uri);

        public abstract Builder title(String title);

        public abstract Builder short_title(String short_title);

        public abstract Builder primary_title_component(String primary_title_component);

        public abstract Builder secondary_title_component(String secondary_title_component);

        public abstract Builder item_cover_renditions(String item_cover_renditions);

        public abstract Builder item_category_id(Long item_category_id);

        public abstract Builder latest_version(Long latest_version);

        public abstract Builder is_obsolete(Long is_obsolete);

        public abstract Item build();
    }
}
