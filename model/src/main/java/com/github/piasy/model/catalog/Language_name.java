package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Language_name {
    public abstract Long _id();

    public abstract Long language_id();

    public abstract Long localization_language_id();

    public abstract String name();

    public static Language_name create(Cursor cursor) {
        return AutoValue_Language_name.createFromCursor(cursor);
    }

    public static Func1<Cursor, Language_name> mapper() {
        return AutoValue_Language_name.MAPPER;
    }


    public static Builder builder() {
        return new AutoValue_Language_name.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder language_id(Long language_id);

        public abstract Builder localization_language_id(Long localization_language_id);

        public abstract Builder name(String name);

        public abstract Language_name build();
    }
}
