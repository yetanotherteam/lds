package com.github.piasy.model.users;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mikhailz on 09/05/16.
 */
public interface ServerApi {

    @GET("addUser.pl")
    Observable<SignUpResponse> signUpWithEmail(
            @Query("username") String username,
            @Query("password") String password,
            @Query("email") String email,
            @Query("udid") String udid);

    @GET("addUser.pl")
    Observable<SignUpResponse> signUpWithFacebook(
            @Query("email") String email,
            @Query("username") String username,
            @Query("fbid") String fbid,
            @Query("fbuser") String fbuser,
            @Query("name") String name,
            @Query("imgurl") String imgurl);

    @GET("login.pl")
    Observable<LoginResponse> loginWithEmail(
            @Query("email") String email,
            @Query("password") String password,
            @Query("udid") String udid,
            @Query(value = "device", encoded = true) String device,
            @Query("appversion") String appVersion);

    @GET("login.pl")
    Observable<LoginResponse> loginWithFacebook(
            @Query("email") String email,
            @Query("username") String username,
            @Query("fbid") String fbid,
            @Query("fbuser") String fbuser,
            @Query("name") String name,
            @Query("imgurl") String imgurl,
            @Query(value = "device", encoded = true) String device,
            @Query("appversion") String appVersion);


    @GET("latestDB.pl?appversion=1.0-13&device=android")
    Observable<DbVersionInfo> getDbVersionInfo();

    @GET("prodb.pl?catalog=224&device=iphone&lang=eng&appversion=1.0-13")
    Observable<BookMeta> getBooksMeta();
//
//    [NSMutableDictionary new];
//    params[@"email"] = email;
//    params[@"username"] = username;
//    params[@"fbid"] = fbID;
//    params[@"fbuser"] = fbName;
//    params[@"name"] = fbName;
//    params[@"imgurl"] = fbImageURL;
//    [self loginWithParams:params resBlock:resBlock];

}
