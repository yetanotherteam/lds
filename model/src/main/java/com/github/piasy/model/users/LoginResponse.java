package com.github.piasy.model.users;

import com.google.auto.value.AutoValue;

import com.ryanharter.auto.value.gson.annotations.AutoGson;

import android.support.annotation.Nullable;

/**
 * Created by mikhailz on 17/05/16.
 */
@AutoValue
@AutoGson(AutoValue_LoginResponse.GsonTypeAdapter.class)
public abstract class LoginResponse {

    @Nullable
    public abstract String imgurl();

    public abstract long userid();

    @Nullable
    public abstract String fbuser();

    public abstract int success();

    public abstract int confirmed();

    public abstract String msg();

    @Nullable
    public abstract String fbid();

    @Nullable
    public abstract String username();

    @Nullable
    public abstract String sid();

    @Nullable
    public abstract String planexpiredate();

    public abstract int subscribed();

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder imgurl(String imgurl);

        public abstract Builder userid(long userid);

        public abstract Builder fbuser(String fbuser);

        public abstract Builder success(int success);

        public abstract Builder confirmed(int confirmed);

        public abstract Builder msg(String msg);

        public abstract Builder fbid(String fbid);

        public abstract Builder username(String username);

        public abstract Builder sid(String sid);

        public abstract Builder planexpiredate(String planexpiredate);

        public abstract Builder subscribed(int subscribed);

        public abstract LoginResponse build();


    }

}
