package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Library_item {

    public abstract Long _id();

    public abstract String external_id();

    public abstract Long library_section_id();

    public abstract String library_section_external_id();

    public abstract Long position();

    public abstract String title();

    public abstract Long is_obsolete();

    public abstract Long item_id();

    public abstract String item_external_id();

    public static Builder builder() {
        return new AutoValue_Library_item.Builder();
    }

    public static Library_item create(Cursor cursor) {
        return AutoValue_Library_item.createFromCursor(cursor);
    }

    public static Func1<Cursor, Library_item> mapper() {
        return AutoValue_Library_item.MAPPER;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder external_id(String external_id);

        public abstract Builder library_section_id(Long library_section_id);

        public abstract Builder library_section_external_id(String library_section_external_id);

        public abstract Builder position(Long position);

        public abstract Builder title(String title);

        public abstract Builder is_obsolete(Long is_obsolete);

        public abstract Builder item_id(Long item_id);

        public abstract Builder item_external_id(String item_external_id);

        public abstract Library_item build();
    }
}
