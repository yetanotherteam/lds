package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import javax.annotation.Nullable;

import rx.functions.Func1;

@AutoValue
public abstract class Nav_section {
    public abstract Long _id();

    @Nullable
    public abstract Long nav_collection_id();

    @Nullable
    public abstract Long position();

    @Nullable
    public abstract String title();

    @Nullable
    public abstract String index_title();

    public static Builder builder() {
        return new AutoValue_Nav_section.Builder();
    }

    public static Nav_section create(Cursor cursor) {
        return AutoValue_Nav_section.createFromCursor(cursor);
    }

    // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
    public static Func1<Cursor, Nav_section> mapper() {
        return AutoValue_Nav_section.MAPPER;
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder nav_collection_id(Long nav_collection_id);

        public abstract Builder position(Long position);

        public abstract Builder title(String title);

        public abstract Builder index_title(String index_title);

        public abstract Nav_section build();
    }

    @Override
    public String toString() {
        return title();
    }
}
