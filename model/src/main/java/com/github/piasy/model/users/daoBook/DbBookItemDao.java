package com.github.piasy.model.users.daoBook;

import com.github.piasy.model.users.BookItemMeta;
import com.github.piasy.model.users.BookMeta;

import android.support.annotation.NonNull;

import java.util.List;

import rx.Observable;

/**
 * Created by mikhailz on 06/06/2016.
 */
public interface DbBookItemDao {

    @NonNull
    Observable<List<BookItemMeta>> getAllBooks();

    Observable<BookMeta> getBooksMeta();

    @NonNull
    Observable<BookItemMeta> getBookById(long id);

    @NonNull
    Observable<Boolean> isNewDbVersion();

    @NonNull
    void saveToDb(List<BookItemMeta> bookItemMetaList);

    @NonNull
    Observable<BookItemMeta> getBookByExternalId(final String externalId);
}
