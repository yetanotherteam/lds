package com.github.piasy.model.dataScheme;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Subitem_content_fts_content {
  public abstract Long docid();
  public abstract String c0subitem_id();
  public abstract String c1content();

  public static Builder builder() {
    return new AutoValue_Subitem_content_fts_content.Builder();
  }

  public static Subitem_content_fts_content create(Cursor cursor) {
    return AutoValue_Subitem_content_fts_content.createFromCursor(cursor);
  }

  // Optional: if your project includes RxJava the extension will generate a Func1<Cursor, User>
  public static Func1<Cursor, Subitem_content_fts_content> mapper() {
    return AutoValue_Subitem_content_fts_content.MAPPER;
  }


  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder docid(Long docid);

    public abstract Builder c0subitem_id(String c0subitem_id);

    public abstract Builder c1content(String c1content);

    public abstract Subitem_content_fts_content build();
  }
}
