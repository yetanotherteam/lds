package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Stopword {

  public abstract Long _id();
  public abstract Long language_id();
  public abstract String word();

  public static Builder builder() {
    return new AutoValue_Stopword.Builder();
  }

  public static Stopword create(Cursor cursor) {
    return AutoValue_Stopword.createFromCursor(cursor);
  }

  public static Func1<Cursor, Stopword> mapper() {
    return AutoValue_Stopword.MAPPER;
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder language_id(Long language_id);

    public abstract Builder word(String word);

    public abstract Stopword build();
  }
}
