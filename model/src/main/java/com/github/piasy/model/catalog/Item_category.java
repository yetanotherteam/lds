package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Item_category {
  public abstract Long _id();
  public abstract String name();

  public static Item_category create(Cursor cursor) {
    return AutoValue_Item_category.createFromCursor(cursor);
  }

  public static Func1<Cursor, Item_category> mapper() {
    return AutoValue_Item_category.MAPPER;
  }

  public static Builder builder() {
    return new AutoValue_Item_category.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder name(String name);

    public abstract Item_category build();
  }
}
