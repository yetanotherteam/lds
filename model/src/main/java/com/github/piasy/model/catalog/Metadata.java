package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Metadata {
  public abstract Long _id();
  public abstract String key();
  public abstract String value();

  public static Builder builder() {
    return new AutoValue_Metadata.Builder();
  }

  public static Metadata create(Cursor cursor) {
    return AutoValue_Metadata.createFromCursor(cursor);
  }

  public static Func1<Cursor, Metadata> mapper() {
    return AutoValue_Metadata.MAPPER;
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder _id(Long _id);

    public abstract Builder key(String key);

    public abstract Builder value(String value);

    public abstract Metadata build();
  }
}
