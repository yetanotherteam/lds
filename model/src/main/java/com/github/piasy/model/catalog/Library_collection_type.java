package com.github.piasy.model.catalog;

import com.google.auto.value.AutoValue;

import android.database.Cursor;

import rx.functions.Func1;

@AutoValue
public abstract class Library_collection_type {
    public abstract Long _id();

    public abstract String name();

    public static Builder builder() {
        return new AutoValue_Library_collection_type.Builder();
    }

    public static Library_collection_type create(Cursor cursor) {
        return AutoValue_Library_collection_type.createFromCursor(cursor);
    }

    public static Func1<Cursor, Library_collection_type> mapper() {
        return AutoValue_Library_collection_type.MAPPER;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder _id(Long _id);

        public abstract Builder name(String name);

        public abstract Library_collection_type build();
    }
}
